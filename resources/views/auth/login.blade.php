@extends('layouts.web')
@section('content')
    <section>
        <article class="top_content">
            <div>
                <form class="form" method="POST" action="{{ route('login') }}">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {{ csrf_field() }}
                        <h1>login</h1>
                        <div>
                            <input id="email" type="email" placeholder="NOMBRE DE USUARIO" name="email"
                                   value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <p><strong>{{ $errors->first('email') }}</strong></p>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div>
                            <input id="password" type="password" placeholder="PASSWORD" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                       <p><strong>{{ $errors->first('password') }}</strong></p>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <button name="login" class="botones" type="submit" id="login">LOGIN</button>
                    <div class="links_into_form">
                        <a href="{{ route('register') }}">aún no tenés cuenta?</a>
                        <a href="{{ route('password.request') }}">olvidaste tus datos de acceso?</a>
                    </div>
                </form>
            </div>
        </article>
    </section>
@endsection

