@extends('layouts.web')

@section('content')
    <section>
        <article class="top_content">
            <div>
                <form class="form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <h1>Restablecer contraseña</h1>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" name="email"
                               placeholder="El email de tu cuenta: ejemplo@gmail.com"
                               value="{{ old('email') }}"
                               required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="botones">
                        Enviar enlace
                    </button>
                </form>
            </div>
        </article>
    </section>
@endsection
