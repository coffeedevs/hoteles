@extends('layouts.web')

@section('content')
    <section>
        <article class="top_content">
            <div id="top_grande">
                <form class="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <h1>Creá tu cuenta</h1>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div>
                            <p>NOMBRE</p>
                            <input id="name" type="text" placeholder="Juan Carlos Maldonado" name="name" value="{{ old('name') }}" required autofocus>
                            @if ($errors->has('name'))
                                <div class="alert alert-success alert-dismissable fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div>
                            <p>EMAIL</p>
                            <input id="email" type="email"  placeholder="maldonado@gmail.com" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <div class="alert alert-success alert-dismissable fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div>
                            <p>PASSWORD</p>
                            <input id="password" type="password" name="password" required>
                            @if ($errors->has('password'))
                                <div class="alert alert-success alert-dismissable fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div>
                        <p>CONFIRMACION DEL PASSWORD</p>
                        <input id="password-confirm" type="password" name="password_confirmation" required>
                    </div>

                    <div>
                        <p>TELEFONO</p>
                        <input type="tel" placeholder="223 5 18 58 58" name="phone" required value="{{ old('phone') }}">
                    </div>
                    <div>
                        <p>CIUDAD</p>
                        <input type="text" placeholder="Mar del Plata" name="city" required value="{{ old('city') }}">
                    </div>
                    <div>
                        <p>PROVINCIA</p>
                        <input type="text" placeholder="Buenos Aires" name="state" required value="{{ old('state') }}">
                    </div>
                    <div>
                        <p>PAÍS</p>
                        <input type="text" placeholder="Argentina" name="country" required value="{{ old('country') }}">
                    </div>
                    <button name="edit_data" class="botones" type="submit" id="edit_data">GUARDAR DATOS</button>
                </form>
                <div class="mensajes_post_form" style="display:none;">
                    <h1>no pudimos guardar los cambios</h1>
                    <p>Por favor, intentalo mas tarde. Si el problema persiste, comunicate con nosotros.</p>
                </div>
                <div class="mensajes_post_form" style="display:none;">
                    <h1>datos guardados con éxito</h1>
                </div>
            </div>
        </article>
    </section>
@endsection
