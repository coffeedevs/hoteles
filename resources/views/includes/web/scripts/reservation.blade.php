<script>
    $(document).on('ready', function () {
        function start_at(start, end, label) {
            console.log(start);
            if (start) {
                start = moment(start, 'DD/MM/YYYY');
                console.log(start);
                $('.start_at p').html('<span>' + start.format("DD") + '</span> /' + start.format("MMM"));
                $('#start_at').val(start.format('DD/MM/YYYY'));
            }
        }

        function finish_at(start, end, label) {
            console.log(start);
            if (start) {
                start = moment(start, 'DD/MM/YYYY');
                console.log(start);
                $('.finish_at p').html('<span>' + start.format("DD") + '</span> /' + start.format("MMM"));
                $('#finish_at').val(start.format('DD/MM/YYYY'));
            }
        }

        $('.start_at .select_date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }
            },
            start_at
        );

        $('.finish_at .select_date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'DD/MM/YYYY'
                }
            },
            finish_at
        );

        start_at($('#start_at').val());
        finish_at($('#finish_at').val());

        $('.toggle_guests').click(function () {
            $('.select_huespedes').toggle();

            if ($('.select_huespedes').css('display') === 'none')
                $('.toggle_guests').text('▼');
            else
                $('.toggle_guests').text('▲');
        });

        $('.select_huespedes a').click(function () {
            var value = $(this).attr('data-value');
            $('#guests_number').val(value);
            $('.guests p').html('<span>' + value + '</span>');
            $('.select_huespedes').hide();
            $('.toggle_guests').text('▼');
        });

        $('.select_product a').click(function () {
            var value = $(this).attr('data-value');
            $('#room_type').val(value);
            $('.select_product a').css('background-color', '#E1A88B');
            $(this).css('background-color', '#6B4C52');
        });

        $('.check-reservation').on('click', function (event) {
            event.preventDefault();
            var form = $(this.form);
            var start = $('input#start_at').val();
            var finish = $('input#finish_at').val();
            var guests = $('input#guests_number').val();

            var mStart = moment(start, "DD/MM/YYYY");
            var mFinish = moment(finish,"DD/MM/YYYY");



            if (start && finish && guests && mFinish.isAfter(mStart)) {
                form.submit();
            } else {
                if(!mFinish.isAfter(mStart)){
                    swal({
                        title: 'Error',
                        text: 'La fecha de finalización debe ser posterior a la de inicio',
                        type: 'error'
                    })
                }else{
                    swal({
                        title: 'Error',
                        text: 'Tienes que completar todos los campos',
                        type: 'error'
                    })
                }
            }
        })
    });
</script>