<div class="selects">
    <div id="no_check" class="start_at">
        <h2 class="destacado_gris">CHECK-IN</h2>
        <p><span>&nbsp;</span></p><a class="select_date">▼</a>
        <input type="hidden" name="start_at" id="start_at" value="{{ Carbon\Carbon::today()->format('d/m/Y') }}">
    </div>
    <div id="vertical_linea"></div>
    <div id="no_check" class="finish_at">
        <h2 class="destacado_gris">CHECK-OUT</h2>
        <p><span>&nbsp;</span></p><a class="select_date">▼</a>
        <input type="hidden" name="finish_at" value="{{ Carbon\Carbon::today()->addWeek()->format('d/m/Y') }}" id="finish_at">
    </div>
    <div id="vertical_linea"></div>
    <div id="no_check" class="guests">
        <h2 class="destacado_gris">HUÉSPEDES</h2>
        <p><span>1</span></p><a class="toggle_guests">▼</a>
        <input type="hidden" name="guests_number" value="1" id="guests_number">
    </div>
</div>

<div class="select_huespedes" style="display:none;">
    <a data-value="1">1</a>
    <a data-value="2">2</a>
    <a data-value="3">3</a>
    <a data-value="4">4</a>
    <a data-value="5">5</a>
    <a data-value="6">6</a>
    <a data-value="7">7</a>
    <a data-value="8">8 o +</a>
</div>
<div class="aclaraciones_reserva">
    <h2>CAMA<span>INDICÁ TUS PREFERENCIAS</span></h2>
    <div>
        <p><input checked id="tipo_cama_matrimonial" name="tipo_cama" value="double"
                  type="radio"> <label for="tipo_cama_matrimonial">MATRIMONIAL</label></p>
    </div>
    <div>
        <p><input name="tipo_cama" id="tipo_cama_individuales" value="single" type="radio">
            <label for="tipo_cama_individuales">INDIVIDUALES</label></p>
    </div>
    <div>
        <p><input id="cama_extra" name="cama_extra" value="extra" type="checkbox"><label
                    for="cama_extra">CAMA EXTRA</label></p>
    </div>
</div>
<div class="aclaraciones_reserva" id="opcionales_reserva">
    <h2>OPCIONALES<span>INDICÁ CANTIDAD DE DÍAS DE SERVICIO</span></h2>
    <div>
        <p>MASAJES >> <input name="massages" value="" type="text"></p>
    </div>
    <div>
        <p>SPA & RELAX >> <input name="spa" value="" type="text"></p>
    </div>
    <div>
        <p>KID'S TIME >> <input name="kids" value="" type="text"></p>
    </div>
</div>
<div class="boton_final_reserva">
    <button id="boton_salmon" class="check-reservation">CHEQUEAR</button>
</div>