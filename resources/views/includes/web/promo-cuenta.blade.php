<section>
    <article id="promo_cuenta">
        <div class="listados" id="bloque_previo">
            <i class="fa fa-user" aria-hidden="true"></i>
            <h2>CREÁ TU CUENTA!</h2>
            <p>Podrás realizar tus reservas online y tener un registro de todas tus transacciones online. Además,
                participarás de sorteos exclusivos y promociones especiales que sólo los usuarios registrados podrásn
                acceder. Hacelo TOTALMENTE GRATIS ahora!</p>
            <a href="{{ route('register') }}" target="_self" class="botones">CREAR CUENTA</a>
        </div>
        <div class="listados" id="bloque_previo">
            <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
            <h2>HACÉ TU RESERVA ONLINE</h2>
            <p>Podés gestionar tu estadía completa de manera fácil y rápida desde el menu principal de <b>USUARIO
                    REGISTRADO, contando con un almanaque online que fija tus reservas de manera remota a nuestra base
                    de datos, y te permite además cancelar o modificar las fechas según sea necesario.</b></p>
        </div>
        <div class="listados" id="bloque_previo">
            <i class="fa fa-history" aria-hidden="true"></i>
            <h2>HISTORIAL DE RESERVAS</h2>
            <p>Además, contarás con un registro constante de tus estadías previas, con el reflejo de tus consumos y
                servicios adicionales aplicados a cada una de tus reservas. <b>De esta manera, vos sabrás cada detalle,
                    y nosotros conoceremos aún más a fondo tus necesidades y deseos, para brindarte así el mejor
                    servicio.</b></p>
        </div>
    </article>
</section>