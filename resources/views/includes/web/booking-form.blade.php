<form action="{{ route('web.reservation-check') }}" method="post">
    {{ csrf_field() }}
    <div class="select_product">
        <div id="hotel">
            <a style="background: #6B4C52" data-value="{{ App\Definitions\Room::NORMAL_TYPE }}">hotel?</a>
        </div>
        <div id="apart">
            <a data-value="apart">o apart?</a>
        </div>
        <input type="hidden" name="room_type" value="{{ App\Definitions\Room::NORMAL_TYPE }}"
               id="room_type">
    </div>
   @include('includes.web.booking-form-base')
</form>