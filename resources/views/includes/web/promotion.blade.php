<div class="listados" id="top">
    <img src="{{ asset('/images/large/' . $promotion->image) }}">
    <div class="content_paquete">
        <h2 class="destacado_gris promotion-header">{{ $promotion->name }}</h2>
        <h2>{{ $promotion->subtitle }}</h2>
        <p class="promotion-description">{{ $promotion->description }}</p>
        <div class="promotion-description">Desde:{{ $promotion->start_at->format('d/m/Y') }}<br>
            Hasta: {{ $promotion->finish_at->format('d/m/Y') }}</div>
        <div class="precios">
            <p>PRECIO REGULAR &gt;&gt; <b>$<span>{{ $promotion->price }}</span>.-</b></p>
            <p><b>PRECIO EXCLUSIVO &gt;&gt; $<span>{{ $promotion->exclusive_price }}</span>.-</b></p>
        </div>
        @if($promotion->isSpent())
            <span style="color:red" class="text-center">AGOTADO</span>
        @else
            <a href="{{ route('web.pay-promotion',$promotion->id) }}" class="botones">ADQUIRIR PAQUETE &gt;&gt;</a>
        @endif


    </div>
</div>

