@include('includes.errors')
@if (Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {!! Session::get('success') !!}
        @if (Session::has('video_success'))
            <span>Puedes verlo haciendo click <a href="{{ route('admin.videos.index') }}">aquí</a>.</span>
        @endif
    </div>
@endif
@if (Session::has('warning'))
    <div class="alert alert-warning">
        {{ Session::get('warning') }}
    </div>
@endif
@if (Session::has('info'))
    <div class="alert alert-info">
        {{ Session::get('info') }}
    </div>
@endif
@if (Session::has('danger'))
    <div class="alert alert-danger">
        {{ Session::get('danger') }}
    </div>
@endif