<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">OPCIONES</li>
            <li @if(Request::url() == route('admin.home')) class="active"@endif>
                <a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>
            <li @if(Request::url() == route('admin.rooms.index')) class="active"@endif>
                <a href="{{ route('admin.rooms.index') }}"><i class="fa fa-key"></i> <span>Habitaciones</span></a>
            </li>
            <li @if(Request::url() == route('admin.bookings.index')) class="active"@endif>
                <a href="{{ route('admin.bookings.index') }}"><i class="fa fa-credit-card"></i> <span>Reservas</span></a>
            </li>
            <li @if(Request::url() == route('admin.promotions.index')) class="active"@endif>
                <a href="{{ route('admin.promotions.index') }}"><i class="fa fa-bullhorn"></i> <span>Promociones</span></a>
            </li>
            <li @if(Request::url() == route('admin.users.index')) class="active"@endif>
                <a href="{{ route('admin.users.index') }}"><i class="fa fa-user"></i> <span>Usuarios</span></a>
            </li>
            {{--<li @if(Request::url() == route('admin.prices.index')) class="active"@endif>--}}
                {{--<a href="{{ route('admin.prices.index') }}"><i class="fa fa-user"></i> <span>Precios</span></a>--}}
            {{--</li>--}}
        </ul>
    </section>
</aside>
