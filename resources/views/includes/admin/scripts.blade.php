<!-- REQUIRED JS SCRIPTS -->
<script src="{{ asset('js/laroute.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/moment-with-locales.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/app.min.js') }}"></script>

<!-- Custom Coffeedevs -->
<script src="{{ asset('assets/admin/custom/js/coffeedevs.js') }}"></script>

<!-- SweetAlert -->
<script src="{{ asset('vendors/bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>

<!-- bootstrap datepicker -->
<script src="{{ asset('assets/admin/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- bootstrap timepicker -->
<!-- bootstrap time picker -->
<script src="{{ asset('assets/admin/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('assets/admin/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>


{{--<!-- fullCalendar 2.2.5 -->--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>--}}
{{--<script src="{{ asset('assets/admin/plugins/fullcalendar/fullcalendar.min.js') }}"></script>--}}


<!-- Fileinput-->
<script src="{{ asset('vendors/fileinput/fileinput.min.js') }}"></script>

<script src="{{ asset('vendors/select2/select2.full.min.js') }}"></script>

<!-- Slimscroll -->
<script src="{{ asset('assets/admin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>

<!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
<!-- Morris Charts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('assets/admin/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('js/helpers.js') }}"></script>



<script>

    $(document).ready(function () {
        //Date picker
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
        moment.locale('es');
    });

    jQuery(window).load(function() {
        jQuery("#loader").delay(500).fadeOut();
    })
</script>