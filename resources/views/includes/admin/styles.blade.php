<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{ asset('assets/admin/bootstrap/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('assets/admin/dist/css/AdminLTE.css') }}">
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
<link rel="stylesheet" href="{{ asset('assets/admin/dist/css/skins/skin-purple-light.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/dist/css/skins/skin-blue.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/daterangepicker/daterangepicker.css') }}">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/datepicker/datepicker3.css') }}">
<!-- timepicker -->
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/timepicker/bootstrap-timepicker.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/admin/plugins/fullcalendar/fullcalendar.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/plugins/fullcalendar/fullcalendar.print.css') }}" media="print">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<link rel="stylesheet" href="{{ asset('vendors/select2/select2.min.css') }}">

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Vendors -->
<link rel="stylesheet" href="{{ asset('vendors/bower_components/sweetalert/dist/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/fileinput/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/custom/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/custom/css/theme.css') }}">
<!-- Morris -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">




<style>
    [v-cloak] .v-cloak--block {
        display: block;
    }
    [v-cloak] .v-cloak--inline {
        display: inline;
    }
    [v-cloak] .v-cloak--inlineBlock {
        display: inline-block;
    }
    [v-cloak] .v-cloak--hidden {
        display: none;
    }
    [v-cloak] .v-cloak--invisible {
        visibility: hidden;
    }
    .v-cloak--block,
    .v-cloak--inline,
    .v-cloak--inlineBlock {
        display: none;
    }


    .av-siteloader-cell {
        display: table-cell;
        vertical-align: middle;
    }

    .av-siteloader-wrap {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;
        z-index: 1000000;
        background: #fff;
    }
    .av-siteloader-inner {
        position: relative;
        display: table;
        width: 100%;
        height: 100%;
        text-align: center;
    }
</style>
