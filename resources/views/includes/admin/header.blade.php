<header class="main-header">
    <a href="{{ url('/') }}" class="logo text-center" style="overflow: visible">
        <span class="logo-mini"><img style="max-height:50px; padding:9px" class="img-responsive" src="{{ asset('/assets/web/images/iso_hotel_sierra_de_los_padres.png') }}"></span>
        <span class="logo-lg">Sierra de los Padres</span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
               <li>
                    <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
