@extends('layouts.web')
@section('content')
    <section>
        <article class="top_content">
            <div>
                <div class="mensajes_post_form" id="post_check" style="display:block;">
                    @if(count($options)>0)
                    <table class="table_opciones">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">HUÉSP.</th>
                            <th scope="col">HABIT.</th>
                            <th scope="col">CHECK-IN</th>
                            <th scope="col">CHECK-OUT</th>
                            <th scope="col">OPCIONALES</th>
                            <th scope="col">PRECIO TOTAL</th>
                            <th scope="col"></th>
                        </tr>
                        @foreach($options as $option)
                            <tr>
                                <td>{{ $option['type']=='normal'?'Hotel':'Apart' }}</td>
                                <td>{{ $option['data']['guests_number'] }}</td>
                                <td>
                                    @foreach($option['rooms'] as $room)
                                        <span>1 hab. p/{{ $room->places }} personas </span>
                                @endforeach
                                <td>{{ $option['data']['start_at'] }}</td>
                                <td>{{ $option['data']['finish_at'] }}</td>
                                <td>
                                    {!! empty($option['additional']['label'])?'-': $option['additional']['label'] !!}
                                </td>
                                <td>${{ $option['total'] }}</td>
                                <td>
                                    <form action="{{ route('web.confirm-booking') }}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="data" value="{{ json_encode($option) }}">
                                        <button class="botones">TOMAR RESERVA</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </table>

                    @else
                        <h1>no hay disponibilidad para tu selección...</h1>
                    @endif

                    @if(count($suggested)>0)

                        <p><b>TAMBIÉN PODÉS TOMAR EN CUENTA ESTAS OPCIONES!</b></p>
                        <table class="table_opciones">
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">HUÉSP.</th>
                                <th scope="col">HABIT.</th>
                                <th scope="col">CHECK-IN</th>
                                <th scope="col">CHECK-OUT</th>
                                <th scope="col">OPCIONALES</th>
                                <th scope="col">PRECIO TOTAL</th>
                                <th scope="col"></th>
                            </tr>
                            @foreach($suggested as $option)
                                <tr>
                                    <td>{{ $option['type']=='normal'?'Hotel':'Apart' }}</td>
                                    <td>{{ $option['data']['guests_number'] }}</td>
                                    <td>
                                        @foreach($option['rooms'] as $room)
                                            <div>1 hab. p/{{ $room->places }} personas </div>
                                        @endforeach
                                    </td>
                                    <td>{{ $option['data']['start_at'] }}</td>
                                    <td>{{ $option['data']['finish_at'] }}</td>
                                    <td>
                                        {!! empty($option['additional']['label'])?'-': $option['additional']['label'] !!}
                                    </td>
                                    <td>${{ $option['total'] }}</td>
                                    <td>
                                        <form action="{{ route('web.confirm-booking') }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="data" value="{{ json_encode($option) }}">
                                            <button class="botones">TOMAR RESERVA</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    @endif


                </div>
            </div>
        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection

@section('scripts')
    @include('includes.web.scripts.reservation')
@endsection
