@extends('layouts.web')
@section('content')
    <section>
        <article class="top" id="header_servicios">
            <div>
                <div id="content_title_page">
                    <h2 class="destacado_gris">SPA & RELAX || KID'S TIME! ||MASAJES PRIVADOS</h2>
                    <h1>Servicios Premium</h1>
                    <p><b>PARA UNA EXPERIENCIA ÚNICA</b></p>
                </div>
            </div>
        </article>
    </section>

    <section>
        <article class="texto_institucional">
            <p>A continuación encontrarás los servicos adicionales disponibles, más aquellos que forman parte innata de
                nuestros productos hoteleros.<br>LOS SERVICIOS PREMIUM SÓLO SE APLICAN ANTES LA CONTRATACIÓN DE
                ESTADÍAS. NO PUEDE SER ABORDADO DE MANERA INDEPENDIENTE DE ESTADÍAS EN HOTEL Y/O APARTS.</p>
        </article>
    </section>

    <section>
        <article class="paquetes" id="servicios">
            <div class="listados" id="top">
                <img src="{{ asset('assets/web/images/servicios/masajes.jpg') }}">
                <div class="content_paquete">
                    <h2 class="destacado_gris">DESCONTRACTURANTES Y RELAJANTES</h2>
                    <h2>MASAJES PRIVADOS</h2>
                    <p>Nuestro plantel de profesionales le brindarán una sesión amplia de masajes según sus deseos y
                        necesidades. Descontracturantes, relajantes, masajes profundos, terapia de puntos, deportivos,
                        prenatales, aromaterapia, piedras calientes, de pies, Shiatsu y Thai.</p>
                    <div class="precios">
                        <p><b>SESIÓN DE MASALES >> $<span>600</span>.-</b></p>
                    </div>
                    <h2 class="destacado_gris">SÓLO SE APLICA A ESTADÍAS. NO PUEDE SER ABORDADO DE MANERA INDEPENDIENTE
                        DE ESTADÍAS EN HOTEL Y/O APARTS.</h2>
                    <a href="{{ route('web.reservations') }}" class="botones">DEFINIR FECHA >></a>
                    <a mp-mode="dftl"
                       href="https://www.mercadopago.com/mla/checkout/start?pref_id=186053040-c53249ce-c6b2-49c9-a4f9-b7937b89feae"
                       name="MP-payButton" class="botones" id="boton_salmon" style="display:none;">COMPRAR DOS DÍAS</a>
                    <script type="text/javascript">
                        (function () {
                            function $MPC_load() {
                                window.$MPC_loaded !== true && (function () {
                                    var s = document.createElement("script");
                                    s.type = "text/javascript";
                                    s.async = true;
                                    s.src = document.location.protocol + "//secure.mlstatic.com/mptools/render.js";
                                    var x = document.getElementsByTagName('script')[0];
                                    x.parentNode.insertBefore(s, x);
                                    window.$MPC_loaded = true;
                                })();
                            }

                            window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;
                        })();
                    </script>
                </div>
            </div>
            <div id="separador_table_cell"></div>
            <div class="listados" id="top">
                <img src="{{ asset('assets/web/images/servicios/spa_&_relax_1.jpg') }}">
                <div class="content_paquete">
                    <h2 class="destacado_gris">PARA CONECTARTE CON VOS MISMO</h2>
                    <h2>SPA & RELAX</h2>
                    <p>Contá con una piscina climatizada de relax, un sauna seco, ducha escocesa y zonas de relajación.
                        Relajate con un servicio exclusivo, adicional a tu habitación de hotel o part.</p>
                    <div class="precios">
                        <p><b>DÍA DE SPA & RELAX >> $<span>600</span>.-</b></p>
                    </div>
                    <h2 class="destacado_gris">SÓLO SE APLICA A ESTADÍAS. NO PUEDE SER ABORDADO DE MANERA INDEPENDIENTE
                        DE ESTADÍAS EN HOTEL Y/O APARTS.</h2>
                    <a href="{{ route('web.reservations') }}" class="botones">DEFINIR FECHA >></a>
                    <a mp-mode="dftl"
                       href="https://www.mercadopago.com/mla/checkout/start?pref_id=186053040-c53249ce-c6b2-49c9-a4f9-b7937b89feae"
                       name="MP-payButton" class="botones" id="boton_salmon" style="display:none;">COMPRAR DOS DÍAS</a>
                    <script type="text/javascript">
                        (function () {
                            function $MPC_load() {
                                window.$MPC_loaded !== true && (function () {
                                    var s = document.createElement("script");
                                    s.type = "text/javascript";
                                    s.async = true;
                                    s.src = document.location.protocol + "//secure.mlstatic.com/mptools/render.js";
                                    var x = document.getElementsByTagName('script')[0];
                                    x.parentNode.insertBefore(s, x);
                                    window.$MPC_loaded = true;
                                })();
                            }

                            window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;
                        })();
                    </script>
                </div>
            </div>
            <div id="separador_table_cell"></div>
            <div class="listados" id="top">
                <img src="{{ asset('assets/web/images/servicios/kids_time_1.jpg') }}">
                <div class="content_paquete">
                    <h2 class="destacado_gris">HASTA LOS 12 AÑOS</h2>
                    <h2>KiD's TiMe!</h2>
                    <p>Servicio recreativo infantil y pre adolescente a cargo de CORPORAL RECREATIVO, que cuenta con más
                        de 18 años de experiencia. <b>SÓLO LOS MIÉRCOLES, VIERNES y SÁBADOS.</b><br>Actividades INDOORS
                        & OUTDOORS.</p>
                    <div class="precios">
                        <p><b>Día completo >> $<span>300</span>.-</b></p>
                    </div>
                    <h2 class="destacado_gris">SÓLO SE APLICA A ESTADÍAS. NO PUEDE SER ABORDADO DE MANERA INDEPENDIENTE
                        DE ESTADÍAS EN HOTEL Y/O APARTS.</h2>
                    <a href="{{ route('web.reservations') }}" class="botones">DEFINIR FECHA >></a>
                    <a mp-mode="dftl"
                       href="https://www.mercadopago.com/mla/checkout/start?pref_id=186053040-c53249ce-c6b2-49c9-a4f9-b7937b89feae"
                       name="MP-payButton" class="botones" id="boton_salmon" style="display:none;">COMPRAR DOS DÍAS</a>
                    <script type="text/javascript">
                        (function () {
                            function $MPC_load() {
                                window.$MPC_loaded !== true && (function () {
                                    var s = document.createElement("script");
                                    s.type = "text/javascript";
                                    s.async = true;
                                    s.src = document.location.protocol + "//secure.mlstatic.com/mptools/render.js";
                                    var x = document.getElementsByTagName('script')[0];
                                    x.parentNode.insertBefore(s, x);
                                    window.$MPC_loaded = true;
                                })();
                            }

                            window.$MPC_loaded !== true ? (window.attachEvent ? window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;
                        })();
                    </script>
                </div>
            </div>
        </article>
    </section>

    <section>
        <article class="texto_institucional">
            <h2>SERVICIOS INCLUÍDOS EN TU ESTADÍA</h2>
            <p>Desayuno, Estacionamiento, wi-fi, Gimnasio, Piscina descubierta, Cancha de Paddle, Juegos de Mesa, Mesa
                de Ping Pong, Metegol.</p>
        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection