@extends('layouts.web')
@section('content')
    <section>
        <article class="top" id="header_paquetes">
            <div>
                <div id="content_title_page">
                    <h2 class="destacado_gris">DESCUENTOS || PROMOCIONES || PAQUETES</h2>
                    <h1>Promociones & Paquetes Octubre</h1>
                    <p><b>OPORTUNIDADES ÚNICAS CON DESCUENTOS IMPERDIBLES</b></p>
                </div>
            </div>
        </article>
    </section>

    <section>
        <article class="texto_institucional">
            <h2>¡NO LO DEJES PASAR!</h2>
            <p>PAGA ONLINE con MERCADOPAGO, en CUOTAS FIJAS con todas las tarjetas de todos los bancos!</p>
            <div id="tarjetas">
                <img src="{{ asset('assets/web/images/tarjetas/master.jpg') }}">
                <img src="{{ asset('assets/web/images/tarjetas/naranja.jpg') }}">
                <img src="{{ asset('assets/web/images/tarjetas/visa.jpg') }}">
                <img src="{{ asset('assets/web/images/tarjetas/pagofacil.jpg') }}">
                <img src="{{ asset('assets/web/images/tarjetas/provincia.jpg') }}">
                <img src="{{ asset('assets/web/images/tarjetas/rapipago.jpg') }}">
            </div>
        </article>
    </section>

    <section>
        <article class="row paquetes" id="servicios">
            @foreach ($promotions as $promotion)
                <div class="col-md-4">
                    @include('includes.web.promotion', ['promotion' => $promotion])
                </div>
            @endforeach
        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection