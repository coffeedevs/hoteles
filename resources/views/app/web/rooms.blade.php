@extends('layouts.web')
@section('styles')
    <style>
        .selects div {
            width: 34%;
        }
    </style>
@endsection
@section('content')
    <section>
        <article class="top" id="habitaciones">
            <div>
                <div id="content_title_page">
                    <h2 class="destacado_gris">17 HABITACIONES DOBLES || 4 HABITACIONES TRIPLES</h2>
                    <h1>Nuestras Habitaciones De Hotel</h1>
                    <p><b>LOS ESPACIOS PENSADOS PARA QUE TENGAS TU TIEMPO...</b></p>
                </div>
            </div>
        </article>
    </section>

    <section>
        <article class="products">
            <div class="content_products">
                <div>
                    <h1>Espacios Únicos</h1>
                    <p><b>Intimidad. Comodidad. Exclusividad.</b></p>
                    <p>Acceso al parque y corredor directo a espacios comunes. Concerjería dedicada a las necesidades
                        individuales de los huéspedes. Ambientes bien equipados. Atractivas vistas de los espacios
                        verdes exteriores.</p>
                </div>
                <img src="{{ asset('assets/web/images/habitaciones/dormitorio.jpg') }}">
            </div>
            <div class="content_products">
                <img src="{{ asset('assets/web/images/habitaciones/living.jpg') }}">
                <div>
                    <h1>Comodidades</h1>
                    <p>Sommiers individuales. Cuna (disponible bajo petición). Cama extra (disponible bajo petición).
                        Pisos: Planta Baja y Primer Piso. Baños completos con bañera y ducha combinadas.</p>
                </div>
            </div>
            <div class="content_products">
                <div>
                    <h1>Servicios & Amenities</h1>
                    <p>Ropa de cama de plumas y edredón. Artículos de aseo de excelente calidad. Toallas y toallones
                        para los huéspedes. Ropa de cama sin plumas disponibles bajo petición.</p>
                    <p>Frigobar, caja de seguridad, televisor LCD, direct TV, wi fi, aire acondicionado, sistema de
                        calefacción por radiadores y losa radiante. Balcón-deck, con vista al jardín y a la calle.</p>
                </div>
                <img src="{{ asset('assets/web/images/habitaciones/bano.jpg') }}">
            </div>

            <div class="filtrado_rapido" id="filtrado_into_products">
                <form action="{{ route('web.reservation-check') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="room_type" value="{{ App\Definitions\Room::NORMAL_TYPE }}"
                           id="room_type">
                    @include('includes.web.booking-form-base')
                </form>
            </div>

            <div class="mensajes_post_form" id="post_check_into_products" style="display:none;">
                <h1>PERFECTO!</h1>
                <p><b>TENEMOS HABITACIÓN PARA VOS.</b></p>
                <button class="botones">TOMAR RESERVA</button>
            </div>
        </article>
    </section>
    @include('includes.web.promo-cuenta')
@endsection

@section('scripts')
    @include('includes.web.scripts.reservation')
@endsection