@extends('layouts.web')
@section('content')
    <section>
        <article class="top_content">
            <div id="top_grande">

                <form class="form" method="post">
                    @include('includes.messages')
                    {{ csrf_field() }}
                    <h1>formulario de contacto</h1>
                    <div>
                        <input value="{{ old('name') }}" type="text" placeholder="Tu nombre y apellido" name="name">
                    </div>
                    <div>
                        <input value="{{ old('email') }}" type="email" placeholder="Tu e-mail" name="email">
                    </div>
                    <div>
                        <input value="{{ old('tel') }}" type="tel" placeholder="Un teléfono de contacto" name="tel">
                    </div>
                    <div>
                        <input value="{{ old('city') }}" type="text" placeholder="Ciudad" name="city">
                    </div>
                    <div>
                        <input value="{{ old('state') }}" type="text" placeholder="Provincia" name="state">
                    </div>
                    <div>
                        <input value="{{ old('country') }}" type="text" placeholder="País" name="country">
                    </div>
                    <div>
                        <textarea name="message" placeholder="Tu mensaje aquí...">{{ old('message') }}</textarea>
                    </div>
                    <button name="formulario_contacto" class="botones" type="submit" id="formulario_contacto">ENVIAR
                        CONSULTA
                    </button>
                </form>
                <div class="mensajes_post_form" style="display:none;">
                    <h1>no pudimos enviar tu consulta...</h1>
                    <p>Por favor, intentalo mas tarde. Si el problema persiste, comunicate con nosotros al 0223 463-0466
                        & 463-1624.</p>
                </div>
                <div class="mensajes_post_form" style="display:none;">
                    <h1>tu mensaje fue enviado con éxito!</h1>
                    <p>Te escribiremos o llamaremos por teléfono a la brevedad. Muchas gracias por tu contacto!</p>
                </div>
            </div>
        </article>
    </section>
    <iframe src="https://snazzymaps.com/embed/34823" style="border:none; width:100%;height:500px"></iframe>
   {{-- <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
    --}}{{--<div class="content_map">--}}{{--
        --}}{{--<div id='gmap_canvas'></div>--}}{{--
        --}}{{--<style>--}}{{--
            --}}{{--#gmap_canvas img {--}}{{--
                --}}{{--max-width: none !important;--}}{{--
                --}}{{--background: none !important--}}{{--
            --}}{{--}--}}{{--
        --}}{{--</style>--}}{{--
    --}}{{--</div>--}}{{--
    <script type='text/javascript'>function init_map() {
            var myOptions = {
                zoom: 14,
                center: new google.maps.LatLng(-37.9420857, -57.776034100000004),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
            marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(-37.9420857, -57.776034100000004)
            });
            infowindow = new google.maps.InfoWindow({content: '<strong>Sierra de los Padres natural hotel & aparts</strong><br>Av. Argentina y Arturo<br>'});
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
            infowindow.open(map, marker);
        }

        google.maps.event.addDomListener(window, 'load', init_map);
    </script>--}}

    @include('includes.web.promo-cuenta')
@endsection