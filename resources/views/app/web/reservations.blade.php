@extends('layouts.web')
@section('content')
    <section>
        <article class="top_content">
            <div>
                <div class="filtrado_rapido" id="filtrado_reservas">
                    @include('includes.web.booking-form')
                </div>
            </div>
        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection

@section('scripts')
    @include('includes.web.scripts.reservation')
@endsection
