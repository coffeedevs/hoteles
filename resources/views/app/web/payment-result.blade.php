@extends('layouts.web')
@section('content')
    <section>
        <article class=" row ">
            <div>
                @if($result=='success')
                    <div class="col-md-6 col-md-offset-3">
                        <div class="alert alert-success fade in alert-dismissable" style="width:50%;margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>Proceso Exitoso!</strong> El hotel se comunicará con Ud. a la brevedad.
                        </div>
                    </div>
                @elseif($result=='pending')
                    <div class="col-md-6 col-md-offset-3">
                        <div class="alert alert-warning fade in alert-dismissable" style="width:50%;margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>Proceso Pendiente!</strong> El hotel se comunicará con Ud. a la brevedad.
                        </div>
                    </div>
                @else
                    <div class="col-md-6 col-md-offset-3">
                        <div class="alert alert-danger fade in alert-dismissable" style="width:50%;margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>Falló el proceso de compra</strong> Intente realizar la compra nuevamente
                        </div>
                    </div>
                @endif
            </div>
        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection