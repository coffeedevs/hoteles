@extends('layouts.web')
@section('content')
    <section>
        <article class="top_content">
            <div>
                <form class="form" method="post">
                    <h1>login</h1>
                    <div>
                        <input type="text" placeholder="NOMBRE DE USUARIO">
                    </div>
                    <div>
                        <input type="password" placeholder="PASSWORD">
                    </div>
                    <button name="login" class="botones" type="submit" id="login">LOGIN</button>
                    <div class="links_into_form">
                        <a href="{{ route('web.register') }}">aún no tenés cuenta?</a>
                        <a href="{{ route('web.recover') }}">olvidaste tus datos de acceso?</a>
                    </div>
                </form>
            </div>
        </article>
    </section>
@endsection