@extends('layouts.web')
@section('content')
    <section>
        <article class="top_content">
            <div>
                <div class="filtrado_rapido" id="filtrado_reservas">
                    <form action="{{ route('web.reservation-check') }}" method="post">
                        {{ csrf_field() }}
                        <div class="select_product">
                            <div id="hotel">
                                <a style="background: #6B4C52" data-value="{{ App\Definitions\Room::NORMAL_TYPE }}">hotel?</a>
                            </div>
                            <div id="apart">
                                <a data-value="apart">o apart?</a>
                            </div>
                            <input type="hidden" name="room_type" value="{{ App\Definitions\Room::NORMAL_TYPE }}"
                                   id="room_type">
                        </div>
                        <div class="selects">
                            <div id="no_check" class="start_at">
                                <h2 class="destacado_gris">CHECK-IN</h2>
                                <p><span>&nbsp;</span></p><a class="select_date">▼</a>
                                <input type="hidden" name="start_at" id="start_at" value="{{ Carbon\Carbon::today()->format('d/m/Y') }}">
                            </div>
                            <div id="vertical_linea"></div>
                            <div id="no_check" class="finish_at">
                                <h2 class="destacado_gris">CHECK-OUT</h2>
                                <p><span>&nbsp;</span></p><a class="select_date">▼</a>
                                <input type="hidden" name="finish_at" id="finish_at">
                            </div>
                            <div id="vertical_linea"></div>
                            <div id="no_check" class="guests">
                                <h2 class="destacado_gris">HUÉSPEDES</h2>
                                <p><span>1</span></p><a class="toggle_guests">▼</a>
                                <input type="hidden" name="guests_number" value="1" id="guests_number">
                            </div>
                        </div>
                        <div class="calendario_completo" style="display:none;">
                            <div id="nav_mes_año">
                                <div style="text-align:left;"><a><i class="fa fa-long-arrow-left"
                                                                    aria-hidden="true"></i></a></div>
                                <div><h2 id="mes" style="text-align:center;">OCTUBRE 2017</h2></div>
                                <div style="text-align:right;"><a><i class="fa fa-long-arrow-right"
                                                                     aria-hidden="true"></i></a></div>
                            </div>

                            <div id="dias">
                                <p>LUNES</p>
                                <p>MARTES</p>
                                <p>MIÉRCOLES</p>
                                <p>JUEVES</p>
                                <p>VIERNES</p>
                                <p>SÁBADO</p>
                                <p>DOMINGO</p>
                            </div>
                            <table class="calendario">
                                <tbody>
                                <tr>
                                    <td><a id="no">25</a></td>
                                    <td><a id="no">26</a></td>
                                    <td><a id="no">27</a></td>
                                    <td><a id="no">28</a></td>
                                    <td><a id="no">29</a></td>
                                    <td><a id="no">30</a></td>
                                    <td><a>1</a></td>
                                </tr>
                                <tr>
                                    <td><a>2</a></td>
                                    <td><a>3</a></td>
                                    <td><a>4</a></td>
                                    <td><a>5</a></td>
                                    <td><a>6</a></td>
                                    <td><a>7</a></td>
                                    <td><a>8</a></td>
                                </tr>
                                <tr>
                                    <td><a>9</a></td>
                                    <td><a>10</a></td>
                                    <td><a>11</a></td>
                                    <td><a>12</a></td>
                                    <td><a>13</a></td>
                                    <td><a>14</a></td>
                                    <td><a>15</a></td>
                                </tr>
                                <tr>
                                    <td><a>16</a></td>
                                    <td><a>17</a></td>
                                    <td><a>18</a></td>
                                    <td><a>19</a></td>
                                    <td><a>20</a></td>
                                    <td><a>21</a></td>
                                    <td><a>22</a></td>
                                </tr>
                                <tr>
                                    <td><a>23</a></td>
                                    <td><a>24</a></td>
                                    <td><a>25</a></td>
                                    <td><a>26</a></td>
                                    <td><a>27</a></td>
                                    <td><a>28</a></td>
                                    <td><a>29</a></td>
                                </tr>
                                <tr>
                                    <td><a>30</a></td>
                                    <td><a>31</a></td>
                                    <td><a id="no">1</a></td>
                                    <td><a id="no">2</a></td>
                                    <td><a id="no">3</a></td>
                                    <td><a id="no">4</a></td>
                                    <td><a id="no">5</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="select_huespedes" style="display:none;">
                            <a data-value="1">1</a>
                            <a data-value="2">2</a>
                            <a data-value="3">3</a>
                            <a data-value="4">4</a>
                            <a data-value="5">5</a>
                            <a data-value="6">6</a>
                            <a data-value="7">7</a>
                            <a data-value="8">8 o +</a>
                        </div>
                        <div class="aclaraciones_reserva">
                            <h2>CAMA<span>INDICÁ TUS PREFERENCIAS</span></h2>
                            <div>
                                <p><input checked id="tipo_cama_matrimonial" name="tipo_cama" value="double"
                                          type="radio"> <label for="tipo_cama_matrimonial">MATRIMONIAL</label></p>
                            </div>
                            <div>
                                <p><input name="tipo_cama" id="tipo_cama_individuales" value="single" type="radio">
                                    <label for="tipo_cama_individuales">INDIVIDUALES</label></p>
                            </div>
                            <div>
                                <p><input id="cama_extra" name="cama_extra" value="extra" type="checkbox"><label
                                            for="cama_extra">CAMA EXTRA</label></p>
                            </div>
                        </div>
                        <div class="aclaraciones_reserva" id="opcionales_reserva">
                            <h2>OPCIONALES<span>INDICÁ CANTIDAD DE DÍAS DE SERVICIO</span></h2>
                            <div>
                                <p>MASAJES >> <input name="opcional" value="" type="text"></p>
                            </div>
                            <div>
                                <p>SPA & RELAX >> <input name="opcional" value="" type="text"></p>
                            </div>
                            <div>
                                <p>KID'S TIME >> <input name="opcional" value="" type="text"></p>
                            </div>
                        </div>
                        <div class="boton_final_reserva">
                            <button id="boton_salmon" class="check-reservation">CHEQUEAR</button>
                        </div>
                    </form>
                </div>

                <div class="mensajes_post_form" id="post_check" style="display:none;">
                    <h1>no hay disponibilidad para tu selección...</h1>
                    <p><b>PERO PODÉS TOMAR EN CUENTA ESTAS OPCIONES!</b></p>
                    <table class="table_opciones">
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">HUÉSP.</th>
                            <th scope="col">HABIT.</th>
                            <th scope="col">CHECK-IN</th>
                            <th scope="col">CHECK-OUT</th>
                            <th scope="col">OPCIONALES</th>
                            <th scope="col">PRECIO TOTAL</th>
                            <th scope="col"></th>
                        </tr>
                        <tr>
                            <td>HOTEL</td>
                            <td>4</td>
                            <td>1</td>
                            <td>05/09/2017</td>
                            <td>10/09/2017</td>
                            <td>
                                <p>- SPA & RELAX</p>
                                <p>- Kid's TIME</p>
                            </td>
                            <td>$5600.-</td>
                            <td>
                                <button class="botones">TOMAR RESERVA</button>
                            </td>
                        </tr>
                        <tr>
                            <td>APART</td>
                            <td>4</td>
                            <td>-</td>
                            <td>05/09/2017</td>
                            <td>10/09/2017</td>
                            <td>
                                <p>- SPA & RELAX</p>
                                <p>- Kid's TIME</p>
                            </td>
                            <td>$5600.-</td>
                            <td>
                                <button class="botones">TOMAR RESERVA</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection

@section('scripts')
    @include('includes.web.scripts.reservation')
@endsection
