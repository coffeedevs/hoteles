@extends('layouts.web')
@section('content')
    <section>
        <article class="top" id="aparts">
            <div>
                <div id="content_title_page">
                    <h2 class="destacado_gris">1 APART PARA 4 PERSONAS || 9 APARTS PARA 6 PERSONAS</h2>
                    <h1>Nuestros Aparts</h1>
                    <p><b>DEPARTAMENTOS IDEALES PARA UN DESCANSO EN FAMILIA</b></p>
                </div>
            </div>
        </article>
    </section>

    <section>
        <article class="products">
            <div class="content_products">
                <div>
                    <h1>Un Complejo Exclusivo</h1>
                    <p><b>10 aparts con 3 ambientes en dos plantas, situados en un predio de 8.000 m2.</b></p>
                    <p>Piscina con solarium, cancha de paddle, sala de juegos con metegol y mesa de ping-pong, y
                        estacionamiento privado.</p>
                </div>
                <img src="{{ asset('assets/web/images/aparts/vista.jpg') }}">
            </div>
            <div class="content_products">
                <img src="{{ asset('assets/web/images/aparts/comedor.jpg') }}">
                <div>
                    <h1>Comodidades</h1>
                    <p>Cada unidad cuenta con alarma individual, servicio de mucama diario y ropa blanca. Cocina
                        totalmente equipada. Comedor. Living. Baño completo privado. Desayuno seco en cada apart. Acceso
                        a todas las instalaciones. Restricciones: no se aceptan mascotas.</p>
                </div>
            </div>
            <div class="content_products">
                <div>
                    <h1>Servicios & Amenities</h1>
                    <p>Ropa de cama de plumas y edredón. Artículos de aseo de excelente calidad. Toallas de felpa y
                        batas para los huéspedes. Ropa de cama sin plumas disponibles bajo petición.</p>
                    <p>Frigobar, caja de seguridad, televisor LCD, direct TV, wi fi, aire acondicionado, sistema de
                        calefacción por radiadores y losa radiante. Balcón-deck, con vista a los jardínes y piscina.</p>
                </div>
                <img src="{{ asset('assets/web/images/aparts/living.jpg') }}">
            </div>

            <div class="filtrado_rapido" id="filtrado_into_products">
                <form action="{{ route('web.reservation-check') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="room_type" value="{{ App\Definitions\Room::APART_TYPE }}"
                           id="room_type">
                    @include('includes.web.booking-form-base')
                </form>
            </div>

            <div class="mensajes_post_form" id="post_check_into_products" style="display:none;">
                <h1>PERFECTO!</h1>
                <p><b>TENEMOS HABITACIÓN PARA VOS.</b></p>
                <button class="botones">TOMAR RESERVA</button>
            </div>

        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection

@section('scripts')
    @include('includes.web.scripts.reservation')
@endsection