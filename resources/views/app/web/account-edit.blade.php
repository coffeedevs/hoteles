@extends('layouts.web')

@section('content')
    <section>
        <article class="top_content">
            <div id="top_grande">
                <form class="form" method="POST" action="{{ route('users.update',Auth::id()) }}">
                    {{ csrf_field() }}
                    <h1>Editá tu cuenta</h1>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div>
                            <p>NOMBRE</p>
                            <input id="name" type="text" name="name" value="{{ $user->name }}" required autofocus>
                            @if ($errors->has('name'))
                                <div class="col-md-3 alert alert-success alert-dismissable fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div>
                            <p>EMAIL</p>
                            <input id="email" type="email" name="email" value="{{ $user->email }}" required disabled>
                            @if ($errors->has('email'))
                                <div class="col-md-3 alert alert-success alert-dismissable fade in">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div>
                        <p>TELEFONO</p>
                        <input type="tel" value="{{ $user->phone }}" name="phone" required>
                    </div>
                    <div>
                        <p>CIUDAD</p>
                        <input type="text" value="{{ $user->city }}" name="city" required>
                    </div>
                    <div>
                        <p>PROVINCIA</p>
                        <input type="text" value="{{ $user->state }}" name="state" required>
                    </div>
                    <div>
                        <p>PAÍS</p>
                        <input type="text" value="{{ $user->country }}" name="country" required>
                    </div>
                    {{ method_field('PUT') }}
                    <button name="edit_data" class="botones" type="submit" id="edit_data">GUARDAR DATOS</button>
                </form>
            </div>
        </article>
    </section>
@endsection
