@extends('layouts.web')
@section('content')
    <section>
        <article class=" row top_content">
            <div>
                <div class="col-md-6 col-md-offset-3">
                    <h1>historial de reservas</h1>
                    <p><b>PARA OTRAS CONSULTAS ESCRIBINOS DESDE <a href="{{ route('web.contacts') }}"><< ACÁ >></a></b>
                    </p>
                    <table class="table_opciones">
                        <tbody>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">HUÉSP.</th>
                            <th scope="col">HABIT.</th>
                            <th scope="col">CHECK-IN</th>
                            <th scope="col">CHECK-OUT</th>
                            <th scope="col">OPCIONALES</th>
                            <th scope="col">ESTADO</th>
                            <th scope="col">PRECIO TOTAL</th>
                            {{--<th scope="col">ACCIONES</th>--}}
                        </tr>
                        @foreach($bookings as $booking)
                            <tr>
                                <td>{{ $booking->type=='apart'?'Apart':'Hotel' }}</td>
                                <td>{{ $booking->guests }}</td>
                                <td>
                                    @foreach($booking->rooms as $room)
                                        <span>1 hab. p/{{ $room->places }} personas </span>
                                    @endforeach
                                </td>
                                <td>{{ $booking->start_at->format('d/m/Y') }}</td>
                                <td>{{ $booking->finish_at->format('d/m/Y') }}</td>
                                <td>
                                    {!! empty($booking->additionalData['label'])?'-': $booking->additionalData['label'] !!}
                                </td>
                                <td title="{{ $booking->status_title }}">{!! $booking->status == App\Booking::STATUS_APPROVED
                                 ? '<i class="fa fa-check"></i>'
                                 : ($booking->status == App\Booking::STATUS_REJECTED ? '<i class="fa fa-cross"></i>' : '') !!} </td>
                                <td>${{ $booking->price }}</td>
                                {{--<td><a class="botones" href="{{ route('web.reservation-edit') }}">EDITAR</a><a--}}
                                {{--class="botones" id="boton_negro" href="#">CANCELAR</a></td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection