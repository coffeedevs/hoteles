@extends('layouts.web')
@section('content')
    <section>
        <article class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-success fade in alert-dismissable" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>La reserva se ha enviado con éxito</strong> En breve se cumunicarán con usted para realizar los próximos pasos.
                    <div>Puede ver los datos de la reserva haciendo click <a href="{{ route('web.history') }}">aquí</a></div>
                </div>
            </div>
        </article>
    </section>

    @include('includes.web.promo-cuenta')
@endsection

@section('scripts')
    @include('includes.web.scripts.reservation')
@endsection
