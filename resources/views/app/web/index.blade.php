@extends('layouts.web')
@section('content')
    <section>
        @if(!empty(Session::get('edit')))
            <div class="col-md-3 alert alert-success alert-dismissable fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                El usuario fué creado con exito.
            </div>
        @endif
        <article class="top" id="top_index">
            <div>
                <div class="special">
                    <h1>SIERRA DE LOS PADRES</h1>
                    <p>natural hotel & aparts</p>
                </div>
            </div>
        </article>
    </section>

    <section>
        <article class="texto_institucional">
            <p><b>Es momento que vuelvas a tu esencia.</b><br>Que puedas volver a conectarte con vos mismo. Que vuelvas
                a
                respirar profundo... volver a tu tiempo.</p>
        </article>
    </section>

    <!--EL RESULTADO DEL SIGUIENTE FILTRADO ESTA EN resultado_filtrado_inicio.html -->

    <section>
        <article class="filtrado_rapido">
            <form method="post" action="{{ route('web.reservation-check') }}">
                {{ csrf_field() }}
                <div class="selects">
                    <div class="start_at">
                        <h2 class="destacado_gris">CHECK-IN</h2>
                        <p><span>&nbsp;</span></p><a class="select_date">▼</a>
                        <input type="hidden" name="start_at" id="start_at"
                               value="{{ Carbon\Carbon::today()->format('d/m/Y') }}">
                    </div>
                    <div id="vertical_linea"></div>
                    <div class="finish_at">
                        <h2 class="destacado_gris">CHECK-OUT</h2>
                        <p><span>&nbsp;</span></p><a class="select_date">▼</a>
                        <input type="hidden" name="finish_at" id="finish_at"
                               value="{{ Carbon\Carbon::today()->addWeek()->format('d/m/Y') }}">
                    </div>
                    <div id="vertical_linea"></div>
                    <div class="guests">
                        <h2 class="destacado_gris">HUÉSPEDES</h2>
                        <p><span>1</span></p><a class="toggle_guests">▼</a>
                        <input type="hidden" name="guests_number" value="1" id="guests_number">
                    </div>
                    <div style="padding:0;" id="div_boton_filtrado">
                        <button id="boton_salmon" class="check-reservation">CHEQUEAR</button>
                    </div>
                </div>
                <div class="select_huespedes" style="display:none;">
                    <a data-value="1">1</a>
                    <a data-value="2">2</a>
                    <a data-value="3">3</a>
                    <a data-value="4">4</a>
                    <a data-value="5">5</a>
                    <a data-value="6">6</a>
                    <a data-value="7">7</a>
                    <a data-value="8">8 o +</a>
                </div>
            </form>
        </article>
    </section>

    @if ($promotions->count() > 0)
        <section>
            <article class="paquetes">
                <div class="listados" id="bloque_previo">
                    <h1>Promociones & Paquetes <span>{{ ucfirst($current_month) }}</span></h1>
                    <p>OPORTUNIDADES ABIERTAS PARA TODOS LOS USUARIOS. <b>APLICABLES DURANTE EL MES
                            DE {{ strtoupper($current_month) }}.</b></p>
                    <p>Cada uno de nuestros paquetes y promociones buscan optimizar y maximizar la experiencia hotelera,
                        el
                        descanso y la libertad que significa ingresar en nuestro mundo. Además, sumamos los mejor
                        servicios
                        hoteleros, junto al profesionalismo y la cordialidad de cada uno de nosotros. Para que tu
                        estadía
                        sea
                        única. Y siempre quieras tener <b>tu tiempo</b>.</p>
                    <a href="{{ route('web.packages')  }}" target="_self" class="botones" id="boton_into_text">TODAS LAS
                        PROMOCIONES</a>
                </div>
                @foreach ($promotions as $promotion)
                    <div id="separador_table_cell"></div>
                    @include('includes.web.promotion', ['promotion' => $promotion])
                @endforeach
            </article>
        </section>
    @endif

    <section>
        <article class="content_promo_products" id="promo_cuenta">
            <div>
                <img src="{{ asset('assets/web/images/index/hotel.jpg') }}">
                <h2 class="destacado_gris">CON IDEAS FRESCAS</h2>
                <h1>Somos un hotel para tu tiempo.</h1>
                <p>Lo que nos hace diferentes no es sólo el entorno natural, que se destaca por las sierras y los prados
                    verdes. Tampoco sólo por nuestros SERVICIOS PREMIUM, entre los que se destacan nuestro SPA & RELAX y
                    piletas climatizadas. Lo que nos hace únicos es nuestra propia identidad. Nuestra forma de pensar y
                    crear. Porque sabemos que lo que necesitás para sentirte bien tenés que sentirlo naturalmente tuyo.
                    Porque sabemos que necesitas un respiro. TU TIEMPO.</p>
                <a href="{{ route('web.rooms') }}" target="_self" class="botones" id="boton_into_text">CONOCÉ NUESTRAS
                    HABITACIONES</a>
            </div>
            <div id="right">
                <img src="{{ asset('assets/web/images/index/aparts.jpg') }}">
                <h2 class="destacado_gris">LIBERTAD TOTAL</h2>
                <h1>Aparts con todos los servicios.</h1>
                <p>El desarrollo de este producto dentro del mismo predio nos ha permitido dar respuesta a las
                    necesidades
                    de muchos huéspedes que necesitan un espacio amplio y adecuado para la familia. Para un máximo de 6
                    personas, nuestros departamentos cuentan con todo lo que necesitás para tener junto a los tuyos de
                    una
                    estadía especial. Contarás además con todos los servicios dispuestos para el hotel, pudiendo elegir
                    libremente los adicionales de los que quieras disfrutar.</p>
                <a href="{{ route('web.aparts') }}" target="_self" class="botones" id="boton_into_text">MIRÁ NUESTROS
                    APARTS</a>
            </div>
        </article>
    </section>

    <section>
        <article class="paquetes" id="servicios">
            <div class="listados" id="bloque_previo">
                <h1>Nuestros Servicios Premium</h1>
                <p>Nuestra premisa es brindarte una estadía única, para que siempre encuentres en <b>SIERRA DE LOS
                        PADRES
                        natural hotel & aparts</b> un espacio donde volver a respirar, y tengas TU TIEMPO.</p>
                <p>Desayunos, estacionamiento exclusivo y protegido, wi-fi en cada espacio privado y de uso común del
                    hotel
                    y nuestros aparts, gimnasio completo, piscina descubierta, masajes, cancha de paddle, juegos de
                    mesa,
                    mesa de ping pong y metegol. Estos son algunos de nuestros servicios, que se suman a nuestro
                    reconocido
                    SPA & RELAX y KID'S TIME!</p>
                <a href="{{ route('web.reservations') }}" target="_self" class="botones" id="boton_into_text">HACÉ TU
                    RESERVA AHORA!</a>
            </div>
            <div id="separador_table_cell"></div>
            <div class="listados" id="paquete">
                <a href="{{ route('web.services') }}" target="_self"><img
                            src="{{ asset('assets/web/images/servicios/spa_&_relax_1.jpg') }}"></a>
                <div class="content_paquete">
                    <h2 class="destacado_gris">PARA CONECTARTE CON VOS MISMO</h2>
                    <h2>SPA & RELAX</h2>
                    <a href="{{ route('web.services') }}" target="_self">+ INFO</a>
                    <div class="precios">
                        <h2>DÍA DE SPA & RELAX >> $<span>600</span>.-</h2>
                    </div>
                    <h2 class="destacado_gris">SÓLO SE APLICA A ESTADÍAS. NO PUEDE SER ABORDADO DE MANERA INDEPENDIENTE
                        DE
                        ESTADÍAS EN HOTEL Y/O APARTS.</h2>
                    <a class="botones" href="{{ route('web.reservations') }}">DEFINIR FECHA >></a>
                </div>
            </div>
            <div id="separador_table_cell"></div>
            <div class="listados" id="paquete">
                <a href="{{ route('web.services') }}" target="_self"><img
                            src="{{ asset('assets/web/images/servicios/kids_time_1.jpg') }}"></a>
                <div class="content_paquete">
                    <h2 class="destacado_gris">HASTA LOS 12 AÑOS</h2>
                    <h2>KiD's TiMe!</h2>
                    <a href="{{ route('web.services') }}" target="_self">+ INFO</a>
                    <div class="precios">
                        <h2>Día completo >> $<span>300</span>.-</h2>
                    </div>
                    <h2 class="destacado_gris">SÓLO SE APLICA A ESTADÍAS. NO PUEDE SER ABORDADO DE MANERA INDEPENDIENTE
                        DE
                        ESTADÍAS EN HOTEL Y/O APARTS.</h2>
                    <a class="botones" href="{{ route('web.reservations') }}">DEFINIR FECHA >></a>
                </div>
            </div>
        </article>
    </section>

    @include('includes.web.promo-cuenta')
    @include('includes.web.testimoniales')
@endsection

@section('scripts')
    @include('includes.web.scripts.reservation')
@endsection
