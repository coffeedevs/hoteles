@extends('layouts.admin')
@section('content-header')
    <section class="content-header">
        <h1>
            Promociones
        </h1>
    </section>
@endsection
@section('content')

        <div class="box box-solid">

            <!-- /.box-header -->
            <div class="box-body">
                <promotion-management
                        :rooms="{{ json_encode(App\Transformers\RoomTransformer::prepare($rooms)['data']) }}"
                        base-route="{{ route('admin.promotions.index') }}"
                        api-route="{{ route('admin.api.promotions.index') }}"
                        :datatable-config="{{ \App\Http\Controllers\Admin\Datatable\PromotionTable::getColumns() }}"
                        :promotion-types="{{ json_encode(\App\Definitions\Room::getTypes()) }}">
                </promotion-management>
            </div>
            <!-- /.box-body -->
        </div>

@endsection