@extends('layouts.admin')
@section('content-header')
    <section class="content-header">
        <h1>
            Reservas
        </h1>
    </section>
@endsection
@section('content')
    <div class="box box-solid">
        <div class="box-body">
            <booking-management
                    base-route="{{ route('admin.bookings.index') }}"
                    api-route="{{ route('admin.api.bookings.index') }}"
                    :datatable-config="{{ \App\Http\Controllers\Admin\Datatable\BookingTable::getColumns() }}">
            </booking-management>
        </div>
    </div>

    <div class="box box-solid">
        <div class="box-header">
            <h2>Revisar disponibilidad de reserva</h2>
        </div>
        <form role="form" method="post" action="{{ route('admin.bookings.available') }}">
            {{ csrf_field() }}
            <div class="box-body">
                @include('includes.messages')
                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <input checked name="room_type" value="normal" type="radio"> HOTEL
                        </label>
                        <label>
                            <input name="room_type" value="apart" type="radio"> APART
                        </label>
                    </div>
                    <div class="form-group">
                        <label>CANT. HUÉSPEDES</label>
                        <input name="guests_number" type="number" value="1" class="form-control" id="exampleInputEmail1"
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label>Desde:</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input value="{{ old('start_at') }}" name="start_at" type="text"
                                   class="form-control pull-right datepicker">
                        </div>
                        <!-- /.input group -->
                    </div>

                    <div class="form-group">
                        <label>Hasta:</label>

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input value="{{ old('finish_at') }}" name="finish_at" type="text"
                                   class="form-control pull-right datepicker">
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>
                            <input checked name="tipo_cama" value="double" type="radio"> CAMA MATRIMONIAL
                        </label>
                        <label>
                            <input name="tipo_cama" value="single" type="radio"> INDIVIDUALES
                        </label>
                    </div>

                    <div class="form-group">
                        <label>MASAJES (días)</label>
                        <input name="massages" type="number" class="form-control" id="exampleInputEmail1"
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label>SPA & RELAX (días)</label>
                        <input name="spa" type="number" class="form-control" id="exampleInputEmail1" placeholder="">
                    </div>
                    <div class="form-group">
                        <label>KID'S TIME (días)</label>
                        <input name="kids" type="number" class="form-control" id="exampleInputEmail1" placeholder="">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input name="cama_extra" value="extra" type="checkbox"> CAMA EXTRA
                        </label>
                    </div>
                </div>


            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Chequear</button>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    @parent
    <script>
        //Date picker
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        })

    </script>

@endsection