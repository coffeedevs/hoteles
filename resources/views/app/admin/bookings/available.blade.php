@extends('layouts.admin')
@section('content-header')
    <section class="content-header">
        <h1>
            Reservas
        </h1>
    </section>
@endsection
@section('content')

    <div class="box box-solid">
        <div class="box-body">
            <article class="top_content">
                <div>
                    <div class="mensajes_post_form" id="post_check" style="display:block;">
                        @if(count($options)>0)
                            <table class="table">
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">HUÉSP.</th>
                                    <th scope="col">HABIT.</th>
                                    <th scope="col">CHECK-IN</th>
                                    <th scope="col">CHECK-OUT</th>
                                    <th scope="col">OPCIONALES</th>
                                    <th scope="col">PRECIO TOTAL</th>
                                    <th scope="col"></th>
                                </tr>
                                @foreach($options as $option)
                                    <tr>
                                        <td>{{ $option['type']=='normal'?'Hotel':'Apart' }}</td>
                                        <td>{{ $option['data']['guests_number'] }}</td>
                                        <td>
                                            @foreach($option['rooms'] as $room)
                                                <span>1 hab. p/{{ $room->places }} personas </span>
                                        @endforeach
                                        <td>{{ $option['data']['start_at'] }}</td>
                                        <td>{{ $option['data']['finish_at'] }}</td>
                                        <td>
                                            {!! empty($option['additional']['label'])?'-': $option['additional']['label'] !!}
                                        </td>
                                        <td>${{ $option['total'] }}</td>
                                        <td>
                                            <form action="{{ route('admin.bookings.confirm-booking') }}" method="post">
                                                {{ csrf_field() }}
                                                Usuario:
                                                <select name="user_id" class="select2">
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->email }}</option>
                                                    @endforeach
                                                </select>
                                                <input type="hidden" name="data" value="{{ json_encode($option) }}">
                                                <button class="btn btn-primary">RESERVAR</button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                        @else
                            <h1>no hay disponibilidad para tu selección...</h1>
                        @endif

                        @if(count($suggested)>0)

                            <p><b>TAMBIÉN PODÉS TOMAR EN CUENTA ESTAS OPCIONES!</b></p>
                            <table class="table">
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">HUÉSP.</th>
                                    <th scope="col">HABIT.</th>
                                    <th scope="col">CHECK-IN</th>
                                    <th scope="col">CHECK-OUT</th>
                                    <th scope="col">OPCIONALES</th>
                                    <th scope="col">PRECIO TOTAL</th>
                                    <th scope="col"></th>
                                </tr>
                                @foreach($suggested as $option)
                                    <tr>
                                        <td>{{ $option['type']=='normal'?'Hotel':'Apart' }}</td>
                                        <td>{{ $option['data']['guests_number'] }}</td>
                                        <td>
                                            @foreach($option['rooms'] as $room)
                                                <div>1 hab. p/{{ $room->places }} personas</div>
                                            @endforeach
                                        </td>
                                        <td>{{ $option['data']['start_at'] }}</td>
                                        <td>{{ $option['data']['finish_at'] }}</td>
                                        <td>
                                            {!! empty($option['additional']['label'])?'-': $option['additional']['label'] !!}
                                        </td>
                                        <td>${{ $option['total'] }}</td>
                                        <td>
                                            <form action="{{ route('admin.bookings.confirm-booking') }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="data" value="{{ json_encode($option) }}">
                                                Usuario:
                                                <select name="user_id" class="select2">
                                                    @foreach($users as $user)
                                                        <option>{{ $user->email }}</option>
                                                    @endforeach
                                                </select>
                                                <button class="btn btn-primary">RESERVAR</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                        @endif

                    </div>
                </div>
            </article>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@section('scripts')
    @parent
    <script src="{{ asset('public/vendors/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2();
    </script>
@endsection

