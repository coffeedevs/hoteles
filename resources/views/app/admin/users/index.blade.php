@extends('layouts.admin')
@section('content-header')
    <section class="content-header">
        <h1>
            Usuarios
        </h1>
    </section>
@endsection
@section('content')
        <div class="box box-solid">
            <div class="box-body">
                <user-management
                        base-route="{{ route('admin.users.index') }}"
                        api-route="{{ route('admin.api.users.index') }}"
                        :datatable-config="{{ \App\Http\Controllers\Admin\Datatable\UserTable::getColumns() }}"
                ></user-management>
            </div>
        </div>
@endsection