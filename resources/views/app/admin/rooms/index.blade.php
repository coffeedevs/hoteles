@extends('layouts.admin')
@section('content-header')
    <section class="content-header">
        <h1>
            Habitaciones
        </h1>
    </section>
@endsection
@section('content')
        <div class="box box-solid">
            <div class="box-body">
                <room-management
                        base-route="{{ route('admin.rooms.index') }}"
                        api-route="{{ route('admin.api.rooms.index') }}"
                        :datatable-config="{{ \App\Http\Controllers\Admin\Datatable\RoomTable::getColumns() }}"
                        :room-types="{{ json_encode(\App\Definitions\Room::getTypes()) }}"
                        :p-prices="{{ json_encode(\App\Transformers\PricesTransformer::prepare(\App\Price::all())['data']) }}">
                </room-management>
            </div>
        </div>
@endsection