@extends('layouts.admin')
@section('content-header')
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
    </section>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-key"></i> </span>

                <a href="{{ route('admin.rooms.index') }}">
                    <div class="info-box-content">
                        <span class="info-box-text">Habitaciones</span>
                        <span class="info-box-number">{{ $count['rooms'] }}</span>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-bullhorn"></i></span>

                <a href="{{ route('admin.promotions.index')}}">
                    <div class="info-box-content">
                        <span class="info-box-text">Promociones</span>
                        <span class="info-box-number">{{ $count['promotions'] }}</span>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-credit-card"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Reservas</span>
                    <span class="info-box-number">{{ $count['bookings'] }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Usuarios</span>
                    <span class="info-box-number">{{ $count['users'] }}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
@endsection