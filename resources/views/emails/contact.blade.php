@component('mail::message')
# Recibimos un nuevo contacto!

El usuario nos dejó los siguientes datos:

Nombre: **{{ $data['name'] }}**

Email: **{{ $data['email'] }}**

Teléfono: **{{ $data['tel'] }}**

Ciudad: **{{ $data['city'] }}**

Provincia: **{{ $data['state'] }}**

País: **{{ $data['country'] }}**

Mensaje: **{{ $data['message'] }}**

Gracias,
{{ config('app.name') }}
@endcomponent
