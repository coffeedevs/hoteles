@component('mail::message')
# Bienvenido {{ $data['name']}}!

Para acceder al estado de su reserva puede ingresar <a href="{{ route('web.history') }}">aquí</a>

Usuario: {{ $data['email'] }}<br>
Password: {{ $data['password'] }}

Gracias,
{{ config('app.name') }}
@endcomponent
