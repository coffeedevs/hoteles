@component('mail::message')
# Recibimos una nueva reserva!

El usuario {{ $booking->name }} realizó una reserva:

Id de reserva : {{ $booking->name }}
Desde : {{ $booking->start_at->format('d/m/Y') }}
Hasta : {{ $booking->finish_at->format('d/m/Y') }}

Podés ver las reservas haciendo click <a href="{{ route('admin.bookings.index') }}">aquí</a>

Gracias,
{{ config('app.name') }}
@endcomponent
