<!DOCTYPE html>
<html>
<head>
    <script>
        window.Laravel =<?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    @section('styles')
        @include('includes.admin.styles')
    @show
    @include('includes.favicon')
</head>
<body class="hold-transition skin-purple-light sidebar-mini fixed">

<div id="loader" class="av-siteloader-wrap av-transition-enabled av-transition-with-logo" >
    <div class="av-siteloader-inner">
        <div class="av-siteloader-cell">
            <i class="fa fa-spinner fa-spin fa-4x" style="color:#605ca8"></i>
        </div>
    </div>
</div>

<div class="wrapper" id="app">
    @include('includes.admin.header')
    @include('includes.admin.sidebar')
    <div class="content-wrapper">
        @yield('content-header')
        <section class="content">
            @yield('content')
        </section>
    </div>
    @include('includes.admin.footer')
</div>
@section('scripts')
    @include('includes.admin.scripts')
@show

</body>
</html>