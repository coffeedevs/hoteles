<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>apart hotel Sierra de los Padres</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Volvé a respirar. Apart Hotel Sierra de los Padres. Tu tiempo.">
    <meta name="keywords"
          content="hotel sierra de los padres, hotel en mar del plata, hotel alejado de la ciudad, spa en mar del plata, apart en mar del plata, cabañas en mar del plata, hotel green en mar del plata, hotel en las sierras, departamento en las sierras, spa en mar del plata, spa en sierra de los padres "/>
    <meta name="author" content="UMM ideas creativas"/>

    <meta property="og:image" content="http://www.hotelsierras.com.ar/images/brand.png">
    <meta property="og:url" content="http://www.hotelsierras.com.ar/">
    <meta property="og:title" content="apart hotel Sierra de los Padres">
    <meta property="og:description" content="Volvé a respirar. Apart Hotel Sierra de los Padres. Tu tiempo. ">

    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/web/css/css.css') }}">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=GFS+Didot" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/web/css/font-awesome.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/web/vendor/bootstrap-daterangepicker-master/daterangepicker.css') }}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.2/sweetalert2.min.css">
    @yield('styles')
</head>
<body>

<header class="header" id="scroll">
    <div id="name">
        <div class="iso">
            <a href="{{ route('web.index') }}" target="_self"><img
                        src="{{ asset('assets/web/images/iso_hotel_sierra_de_los_padres.png') }}"></a>
        </div>
        <h1>apart hotel sierra de los padres. <span>tu tiempo.</span></h1>
    </div>

    <nav class="menu_user_scoll">
        <ul class="topnav" id="myTopnav">
            <li class="icon"><a href="javascript:void(0);" onclick="myFunction()" id="bars"><i
                            class="fa fa-bars"></i></a></li>
            <li><a id="separador"></a></li>
            <li>
                @if (auth()->check())
                    <a href="{{ route('profile') }}">
                        BIENVENIDO, {{ auth()->user()->name }}
                    </a>
                @else
                    <a id="login" href="{{ route('login') }}" target="_self">LOGIN /
                        <a id="create_account" href="{{ route('register') }}" target="_self">/
                            CREAR CUENTA</a>
                    </a>
                @endif
            </li>
            <li><a id="separador"></a></li>

            @if(auth()->check())
                <li><a id="logeado" href="{{ route('web.reservations') }}">HACÉ TU RESERVA ONLINE</a></li>
                <li><a id="logeado" href="{{ route('web.history') }}">HISTORIAL DE RESERVAS</a></li>
                <li><a id="logeado" href="{{ route('profile') }}">EDITAR CUENTA</a></li>
            @endif
            <li><a href="{{ route('web.rooms') }}">HABITACIONES</a></li>
            <li><a href="{{ route('web.services') }}">SERVICIOS PREMIUM</a></li>
            <li><a href="{{ route('web.packages') }}">PROMOCIONES & PAQUETES</a></li>
            <li><a href="{{ route('web.aparts') }}">APARTS</a></li>
            <li><a href="{{ route('web.contacts') }}">CONTACTO</a></li>
            @if(auth()->check() && auth()->user()->hasRole('admin'))
                <li><a href="{{ route('admin.home') }}">ADMIN</a></li>
            @endif
            @if (auth()->check())
                <li><a href="{{ route('logout') }}">CERRAR SESIÓN</a></li>
            @endif
        </ul>
    </nav>
</header>

@yield('content')

<footer>
    <div class="content_footer">
        <img src="{{ asset('assets/web/images/iso_footer_hotel_sierra_de_los_padres.png') }}">
        <div class="special" id="special_footer">
            <h1>SIERRA DE LOS PADRES</h1>
            <p>natural hotel & aparts</p>
        </div>
        <div id="social">
            <a href="https://www.facebook.com/ApartHotelSierraDeLosPadres/" target="_blank"><img
                        src="{{ asset('assets/web/images/facebook.jpg') }}"></a>
            <a href="#" target="_blank"><img src="{{ asset('assets/web/images/instagram.jpg') }}"></a>
            <a href="http://www.booking.com/Share-239NVk" target="_blank"><img
                        src="{{ asset('assets/web/images/booking.jpg') }}"></a>
            <a href="https://www.tripadvisor.com.ar/Hotel_Review-g2167976-d2100196-Reviews-Apart_Hotel_Sierra_de_los_Padres-Sierra_de_los_Padres_Province_of_Buenos_Aires_Centra.html"
               target="_blank"><img src="{{ asset('assets/web/images/tripadvisor.jpg') }}"></a>
            <a href="https://www.despegar.com.ar/hoteles/h-361809/apart-hotel-sierra-de-los-padres-sierra+de+los+padres"
               target="_blank"><img src="{{ asset('assets/web/images/despegar.jpg') }}"></a>
        </div>
        <div id="datos">
            <p>Tel. de contacto >> 0223 463-0466 & 463-1624 || Av. Argentina y Arturo | Sierra de los Padres</p>
            <a href="http://www.hotelsierradelospadres.com.ar" target="_self">hotelsierradelospadres.com.ar</a>
            <p id="no_mail">reservas@hotelsierradelospadres.com.ar - administracion@hotelsierradelospadres.com.ar</p>
        </div>
    </div>
</footer>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{ asset('assets/web/js/header.js') }}"></script>
<script src="{{ asset('assets/web/js/myFunction.js') }}"></script>
<script src="{{ asset('assets/web/vendor/bootstrap-daterangepicker-master/moment.js') }}"></script>
<script src="{{ asset('assets/web/vendor/bootstrap-daterangepicker-master/daterangepicker.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.2/sweetalert2.min.js"></script>
@yield('scripts')
</body>
</html>