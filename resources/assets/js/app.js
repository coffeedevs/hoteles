
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.ElementUI=require('element-ui');
import locale from 'element-ui/lib/locale/lang/es'

Vue.use(ElementUI, { locale });

require('element-ui/lib/theme-default/index.css');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 * Commons
 */
Vue.component('modal', require('./components/admin/commons/Modal.vue'));
Vue.component('loader', require('./components/admin/commons/Loader.vue'));

Vue.component('data-table', require('./components/admin/datatable/Datatable.vue'));
Vue.component('actions', require('./components/admin/datatable/Actions.vue'));
Vue.component('room-management', require('./components/admin/rooms/RoomManagement.vue'));
Vue.component('room-form', require('./components/admin/rooms/RoomForm.vue'));
Vue.component('room-prices-form', require('./components/admin/rooms/RoomPricesForm.vue'));
Vue.component('room-prices-tab', require('./components/admin/rooms/RoomPricesTab.vue'));
Vue.component('booking-management', require('./components/admin/bookings/BookingManagement.vue'));
Vue.component('booking-form', require('./components/admin/bookings/BookingForm.vue'));
Vue.component('promotion-management', require('./components/admin/promotions/PromotionManagement.vue'));
Vue.component('promotion-form', require('./components/admin/promotions/PromotionForm.vue'));
Vue.component('user-management', require('./components/admin/users/UserManagement.vue'));
Vue.component('user-form', require('./components/admin/users/UserForm.vue'));

Vue.component('vue-img-preview', require('./components/admin/commons/ImagePreview.vue'));

window.Event = new class {
    constructor() {
        this.vue = new Vue();
    }

    fire(event, data = null) {
        this.vue.$emit(event, data);
    }

    listen(event, callback) {
        this.vue.$on(event, callback);
    }
};
const app = new Vue({
    el: '#app'
});
