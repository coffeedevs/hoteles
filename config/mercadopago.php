<?php

return [

    'client_id'=>env('MP_CLIENT_ID'),
    'client_secret'=>env('MP_CLIENT_SECRET'),
    'short_name'=>env('MP_SHORT_NAME'),
    'token'=>env('MP_TOKEN'),

];