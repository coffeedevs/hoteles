<?php


Route::group([
    'as' => 'web.',
    'namespace' => 'Web',
], function () {
    Route::view('/', 'app.web.index')->name('index');
    //Route::view('account-edit', 'app.web.account-edit')->name('account-edit');
    Route::get('historial', 'BookingsController@history')->name('history');
    Route::view('reservas', 'app.web.reservations')->name('reservations');
    Route::get('reserva-promocion/{promotion}', 'PromotionsController@bookingPromotion')->name('reservations.promotions');
    Route::post('comprobar-reserva', 'BookingsController@checkAvailability')->name('reservation-check');
    Route::view('reserva-realizada', 'app.web.reservation-success')->name('reservation-success');
    Route::view('editar-reserva', 'app.web.reservation-edit')->name('reservation-edit');
    Route::view('habitaciones', 'app.web.rooms')->name('rooms');
    Route::view('servicios', 'app.web.services')->name('services');
    Route::view('aparts', 'app.web.aparts')->name('aparts');
    Route::view('contacto', 'app.web.contacts')->name('contacts');
    Route::post('contacto', 'ContactsController@send')->name('contacts');
    Route::get('paquetes', 'PromotionsController@getPromotions')->name('packages');

    Route::post('confirm-booking', 'BookingsController@confirmBooking')->name('confirm-booking');
    Route::post('payment-ipn', 'PaymentsController@paymentReceived')->name('payment-received');
    Route::get('payment-success', 'PaymentsController@paymentSuccess')->name('payment-success');
    Route::get('payment-pending', 'PaymentsController@paymentPending')->name('payment-pending');
    Route::get('payment-failure', 'PaymentsController@paymentFailure')->name('payment-failure');


    Route::group([
        'middleware' => 'auth'
    ], function () {
        Route::get('pay-promotion/{promotion}', 'PaymentsController@payPromotion')->name('pay-promotion');
    });
});

Route::get('/inicio', 'HomeController@index')->name('home');
Route::get('mi-perfil', 'UsersController@profile')->name('profile');
Route::resource('users', 'UsersController', ['only' => ['edit', 'update']]);

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
