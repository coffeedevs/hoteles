<?php

Route::group([
    'prefix'     => 'admin',
    'namespace'  => 'Admin',
    'as'         => 'admin.',
    'middleware' => [
        'auth',
        'role:admin',
    ],
], function () {
    Route::get('/', 'DashboardController@index')->name('home');
    Route::get('rooms', 'RoomsController@index')->name('rooms.index');
    Route::get('bookings', 'BookingsController@index')->name('bookings.index');
    Route::post('booking-available','BookingsController@available')->name('bookings.available');
    Route::post('booking-confirm','BookingsController@confirmBooking')->name('bookings.confirm-booking');
    Route::get('promotions', 'PromotionsController@index')->name('promotions.index');
    Route::get('users', 'UsersController@index')->name('users.index');
    Route::get('prices', 'PricesController@index')->name('prices.index');

    Route::group([
        'prefix'    => 'api',
        'namespace' => 'Api',
        'as'        => 'api.',
    ], function () {
        Route::resource('rooms', 'RoomsController');
        Route::resource('bookings', 'BookingsController');
        Route::resource('promotions', 'PromotionsController');
        Route::put('bookings/{booking}/approve', 'BookingsController@approve')->name('bookings.approve');
        Route::put('bookings/{booking}/disapprove', 'BookingsController@disapprove')->name('bookings.disapprove');
        Route::resource('users', 'UsersController');
        Route::get('prices', 'RoomsController@getPrices')->name('prices.get');
        Route::put('prices', 'RoomsController@updatePrices')->name('prices.update');
    });
});
