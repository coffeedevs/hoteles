(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/open","name":"debugbar.openhandler","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@handle"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/clockwork\/{id}","name":"debugbar.clockwork","action":"Barryvdh\Debugbar\Controllers\OpenHandlerController@clockwork"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/stylesheets","name":"debugbar.assets.css","action":"Barryvdh\Debugbar\Controllers\AssetController@css"},{"host":null,"methods":["GET","HEAD"],"uri":"_debugbar\/assets\/javascript","name":"debugbar.assets.js","action":"Barryvdh\Debugbar\Controllers\AssetController@js"},{"host":null,"methods":["GET","HEAD"],"uri":"images\/{template}\/{filename}","name":"imagecache","action":"Intervention\Image\ImageCacheController@getResponse"},{"host":null,"methods":["GET","HEAD"],"uri":"api\/user","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":"web.index","action":"\Illuminate\Routing\ViewController"},{"host":null,"methods":["GET","HEAD"],"uri":"historial","name":"web.history","action":"App\Http\Controllers\Web\BookingsController@history"},{"host":null,"methods":["GET","HEAD"],"uri":"reservas","name":"web.reservations","action":"\Illuminate\Routing\ViewController"},{"host":null,"methods":["GET","HEAD"],"uri":"reserva-promocion\/{promotion}","name":"web.reservations.promotions","action":"App\Http\Controllers\Web\PromotionsController@bookingPromotion"},{"host":null,"methods":["POST"],"uri":"comprobar-reserva","name":"web.reservation-check","action":"App\Http\Controllers\Web\BookingsController@checkAvailability"},{"host":null,"methods":["GET","HEAD"],"uri":"reserva-realizada","name":"web.reservation-success","action":"\Illuminate\Routing\ViewController"},{"host":null,"methods":["GET","HEAD"],"uri":"editar-reserva","name":"web.reservation-edit","action":"\Illuminate\Routing\ViewController"},{"host":null,"methods":["GET","HEAD"],"uri":"habitaciones","name":"web.rooms","action":"\Illuminate\Routing\ViewController"},{"host":null,"methods":["GET","HEAD"],"uri":"servicios","name":"web.services","action":"\Illuminate\Routing\ViewController"},{"host":null,"methods":["GET","HEAD"],"uri":"aparts","name":"web.aparts","action":"\Illuminate\Routing\ViewController"},{"host":null,"methods":["GET","HEAD"],"uri":"contacto","name":"web.contacts","action":"\Illuminate\Routing\ViewController"},{"host":null,"methods":["POST"],"uri":"contacto","name":"web.contacts","action":"App\Http\Controllers\Web\ContactsController@send"},{"host":null,"methods":["GET","HEAD"],"uri":"paquetes","name":"web.packages","action":"App\Http\Controllers\Web\PromotionsController@getPromotions"},{"host":null,"methods":["POST"],"uri":"confirm-booking","name":"web.confirm-booking","action":"App\Http\Controllers\Web\BookingsController@confirmBooking"},{"host":null,"methods":["GET","HEAD"],"uri":"pay-promotion\/{promotion}","name":"web.pay-promotion","action":"App\Http\Controllers\Web\PaymentsController@payPromotion"},{"host":null,"methods":["GET","HEAD"],"uri":"inicio","name":"home","action":"App\Http\Controllers\HomeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"mi-perfil","name":"profile","action":"App\Http\Controllers\UsersController@profile"},{"host":null,"methods":["GET","HEAD"],"uri":"users\/{user}\/edit","name":"users.edit","action":"App\Http\Controllers\UsersController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"users\/{user}","name":"users.update","action":"App\Http\Controllers\UsersController@update"},{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"App\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"App\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"register","name":"register","action":"App\Http\Controllers\Auth\RegisterController@showRegistrationForm"},{"host":null,"methods":["POST"],"uri":"register","name":null,"action":"App\Http\Controllers\Auth\RegisterController@register"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":null,"action":"App\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"admin","name":"admin.home","action":"App\Http\Controllers\Admin\DashboardController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/rooms","name":"admin.rooms.index","action":"App\Http\Controllers\Admin\RoomsController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/bookings","name":"admin.bookings.index","action":"App\Http\Controllers\Admin\BookingsController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/promotions","name":"admin.promotions.index","action":"App\Http\Controllers\Admin\PromotionsController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/users","name":"admin.users.index","action":"App\Http\Controllers\Admin\UsersController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/prices","name":"admin.prices.index","action":"App\Http\Controllers\Admin\PricesController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/rooms","name":"admin.api.rooms.index","action":"App\Http\Controllers\Admin\Api\RoomsController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/rooms\/create","name":"admin.api.rooms.create","action":"App\Http\Controllers\Admin\Api\RoomsController@create"},{"host":null,"methods":["POST"],"uri":"admin\/api\/rooms","name":"admin.api.rooms.store","action":"App\Http\Controllers\Admin\Api\RoomsController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/rooms\/{room}","name":"admin.api.rooms.show","action":"App\Http\Controllers\Admin\Api\RoomsController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/rooms\/{room}\/edit","name":"admin.api.rooms.edit","action":"App\Http\Controllers\Admin\Api\RoomsController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"admin\/api\/rooms\/{room}","name":"admin.api.rooms.update","action":"App\Http\Controllers\Admin\Api\RoomsController@update"},{"host":null,"methods":["DELETE"],"uri":"admin\/api\/rooms\/{room}","name":"admin.api.rooms.destroy","action":"App\Http\Controllers\Admin\Api\RoomsController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/bookings","name":"admin.api.bookings.index","action":"App\Http\Controllers\Admin\Api\BookingsController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/bookings\/create","name":"admin.api.bookings.create","action":"App\Http\Controllers\Admin\Api\BookingsController@create"},{"host":null,"methods":["POST"],"uri":"admin\/api\/bookings","name":"admin.api.bookings.store","action":"App\Http\Controllers\Admin\Api\BookingsController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/bookings\/{booking}","name":"admin.api.bookings.show","action":"App\Http\Controllers\Admin\Api\BookingsController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/bookings\/{booking}\/edit","name":"admin.api.bookings.edit","action":"App\Http\Controllers\Admin\Api\BookingsController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"admin\/api\/bookings\/{booking}","name":"admin.api.bookings.update","action":"App\Http\Controllers\Admin\Api\BookingsController@update"},{"host":null,"methods":["DELETE"],"uri":"admin\/api\/bookings\/{booking}","name":"admin.api.bookings.destroy","action":"App\Http\Controllers\Admin\Api\BookingsController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/promotions","name":"admin.api.promotions.index","action":"App\Http\Controllers\Admin\Api\PromotionsController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/promotions\/create","name":"admin.api.promotions.create","action":"App\Http\Controllers\Admin\Api\PromotionsController@create"},{"host":null,"methods":["POST"],"uri":"admin\/api\/promotions","name":"admin.api.promotions.store","action":"App\Http\Controllers\Admin\Api\PromotionsController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/promotions\/{promotion}","name":"admin.api.promotions.show","action":"App\Http\Controllers\Admin\Api\PromotionsController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/promotions\/{promotion}\/edit","name":"admin.api.promotions.edit","action":"App\Http\Controllers\Admin\Api\PromotionsController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"admin\/api\/promotions\/{promotion}","name":"admin.api.promotions.update","action":"App\Http\Controllers\Admin\Api\PromotionsController@update"},{"host":null,"methods":["DELETE"],"uri":"admin\/api\/promotions\/{promotion}","name":"admin.api.promotions.destroy","action":"App\Http\Controllers\Admin\Api\PromotionsController@destroy"},{"host":null,"methods":["PUT"],"uri":"admin\/api\/bookings\/{booking}\/approve","name":"admin.api.bookings.approve","action":"App\Http\Controllers\Admin\Api\BookingsController@approve"},{"host":null,"methods":["PUT"],"uri":"admin\/api\/bookings\/{booking}\/disapprove","name":"admin.api.bookings.disapprove","action":"App\Http\Controllers\Admin\Api\BookingsController@disapprove"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/users","name":"admin.api.users.index","action":"App\Http\Controllers\Admin\Api\UsersController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/users\/create","name":"admin.api.users.create","action":"App\Http\Controllers\Admin\Api\UsersController@create"},{"host":null,"methods":["POST"],"uri":"admin\/api\/users","name":"admin.api.users.store","action":"App\Http\Controllers\Admin\Api\UsersController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/users\/{user}","name":"admin.api.users.show","action":"App\Http\Controllers\Admin\Api\UsersController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/users\/{user}\/edit","name":"admin.api.users.edit","action":"App\Http\Controllers\Admin\Api\UsersController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"admin\/api\/users\/{user}","name":"admin.api.users.update","action":"App\Http\Controllers\Admin\Api\UsersController@update"},{"host":null,"methods":["DELETE"],"uri":"admin\/api\/users\/{user}","name":"admin.api.users.destroy","action":"App\Http\Controllers\Admin\Api\UsersController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"admin\/api\/prices","name":"admin.api.prices.get","action":"App\Http\Controllers\Admin\Api\RoomsController@getPrices"},{"host":null,"methods":["PUT"],"uri":"admin\/api\/prices","name":"admin.api.prices.update","action":"App\Http\Controllers\Admin\Api\RoomsController@updatePrices"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

