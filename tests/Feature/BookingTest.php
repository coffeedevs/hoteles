<?php

namespace Tests\Feature;

use App\Booking;
use App\BookingRoom;
use App\Http\Middleware\VerifyCsrfToken;
use App\Room;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BookingTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIfThereIsAvailability()
    {
        $this->makeRooms();
        $user = factory(User::class)->create();
        $this->actingAs($user);

        /** @var Carbon $carbon */
        $now = Carbon::now();

        $booking = factory(Booking::class)->create([
            'user_id'     => auth()->user()->id,
            'start_at'    => $now->subDays(20),
            'finish_at'   => $now->addDays(20),
            'children'    => 2,
            'minors'      => true,
            'cradle'      => false,
            'observation' => '',
        ]);

        $rooms = Room::where('type', 'normal')->where('double', 1)->get();

        foreach ($rooms as $room) {
            $booking->rooms()->attach($room);
        }

        $this->withoutMiddleware([VerifyCsrfToken::class]);

        $response = $this->post(route('web.reservation-check', [
            'start_at'      => $now->subDays(10)->format('d/m/Y'),
            'finish_at'     => $now->addDays(10)->format('d/m/Y'),
            'guests_number' => 5,
        ]));

        $preferred = $response->getContent();
        $suggested = $response->getContent();

        //$this->assertEquals()
    }
}
