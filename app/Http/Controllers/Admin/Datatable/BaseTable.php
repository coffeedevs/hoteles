<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 12/2/2017
 * Time: 5:43 PM
 */

namespace App\Http\Controllers\Admin\Datatable;

/** @method static getColumns */
class BaseTable
{
    protected $columns = [];

    public function columns()
    {
        return json_encode($this->columns);
    }

    public static function __callStatic($name, $arguments)
    {
        if($name=='getColumns')
            return call_user_func_array(
                array(new static, 'columns'),
                $arguments
            );
    }
}