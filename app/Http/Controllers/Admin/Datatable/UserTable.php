<?php
namespace App\Http\Controllers\Admin\Datatable;

class UserTable extends BaseTable
{
    protected $columns=[
        [
            'title'=>'Id',
            'name'=>'id',
        ],
        [
            'title'=>'Nombre',
            'name'=>'name',
        ],
        [
            'title'=>'Email',
            'name'=>'email',
        ],
    ];
}