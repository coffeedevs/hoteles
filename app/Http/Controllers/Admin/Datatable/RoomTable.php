<?php
namespace App\Http\Controllers\Admin\Datatable;

class RoomTable extends BaseTable
{
    protected $columns=[
        [
            'title'=>'Número',
            'name'=>'number',
        ],
        [
            'title'=>'Plazas',
            'name'=>'places',
        ],
        [
            'title'=>'Camas dobles',
            'name'=>'double',
        ],
    ];
}