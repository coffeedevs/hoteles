<?php

namespace App\Http\Controllers\Admin\Datatable;

class BookingTable extends BaseTable
{
    protected $columns = [
        [
            'title' => 'Creado',
            'name' => 'created_at',
        ],
        [
            'title' => 'Usuario',
            'name' => 'user',
            'type'=>'slot'
        ],
        [
            'title' => 'Desde',
            'name' => 'start_at',
            'type' => 'date',
        ],
        [
            'title' => 'Hasta',
            'name' => 'finish_at',
            'type' => 'date',
        ],
        [
            'title' => 'Invitados',
            'name' => 'guests',
            'type' => 'integer',
        ],

        [
            'title' => 'Estado',
            'name' => 'status',
        ],

    ];
}