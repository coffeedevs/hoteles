<?php
namespace App\Http\Controllers\Admin\Datatable;

class PromotionTable extends BaseTable
{
    protected $columns=[
        [
            'title'=>'',
            'name'=>'image',
            'type'=>'slot',
        ],
        [
            'title'=>'Nombre',
            'name'=>'name',
        ],
        [
            'title'=>'Subtítulo',
            'name'=>'subtitle',
        ],
        [
            'title'=>'Descripción',
            'name'=>'description',
        ],
        [
            'title'=>'Precio Exclusivo',
            'name'=>'exclusive_price',
        ],
        [
            'title'=>'Precio',
            'name'=>'price',
        ],
    ];
}