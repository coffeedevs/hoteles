<?php

namespace App\Http\Controllers\Admin;

use App\Booking;
use App\Http\Controllers\Controller;
use App\Promotion;
use App\Room;
use App\User;


class DashboardController extends Controller
{
    public function index(){

        $count=[];
        $count['rooms'] = Room::count();
        $count['promotions'] = Promotion::count();
        $count['bookings'] = Booking::count();
        $count['users'] = User::count();



        return view('app.admin.home')->withCount($count);
    }
}
