<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Requests\RoomStoreRequest;
use App\Http\Requests\RoomUpdateRequest;
use App\Price;
use App\Room;
use App\Transformers\PricesTransformer;
use App\Transformers\RoomTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoomsController extends Controller
{
    public function index(Request $request)
    {
        $rooms = Room::query();
        if ($search = $request->get('search'))
            $rooms->where('number', 'like', '%' . $search . '%');

        if ($orderName = $request->get('order_name'))
            $rooms->orderBy($orderName, $request->get('order_type'));
        $rooms = $rooms->paginate(5);

        return RoomTransformer::prepare($rooms);
    }

    public function store(RoomStoreRequest $request)
    {
        $room = new Room();
        $this->fill($request, $room);
        $room->save();

        return $room;
    }

    public function update(RoomUpdateRequest $request, Room $room)
    {
        $this->fill($request, $room);
        $room->save();
        return $room;
    }

    public function destroy(Room $room)
    {
        $room->delete();
        return ['ok'];
    }

    public function fill(Request $request, Room $room)
    {
        $room->number = $request->input('number');
        $room->places = $request->input('places');
        $room->double = $request->input('double');
        $room->type = $request->input('type');
    }

    public function getPrices()
    {
        return PricesTransformer::prepare(Price::all())['data'];
    }

    public function updatePrices(Request $request)
    {

        foreach($request->get('prices') as $p){

            $price = Price::where('month',$p['month'])->first();
            $price->normal2 = $p['normal2'];
            $price->normal3 = $p['normal3'];
            $price->apart4 = $p['apart4'];
            $price->apart6 = $p['apart6'];
            $price->spa = $p['spa'];
            $price->kids = $p['kids'];
            $price->massages = $p['massages'];
            $price->save();
        }

        return ['ok'];
    }
}
