<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Mail\SendPassword;
use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::query();
        if ($search = $request->get('search'))
            $users
                ->where('name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%');

        if ($orderName = $request->get('order_name'))
            $users->orderBy($orderName, $request->get('order_type'));
        $users = $users->paginate(5);

        return UserTransformer::prepare($users);
    }

    public function store(UserStoreRequest $request)
    {
        $user = new User();
        $this->fill($request, $user);
        $password = $request->input('password');
        $user->password = bcrypt($password);
        $user->save();


        Mail::to($user->email)->send(new SendPassword([
            'name'=>$user->name,
            'password'=>$password,
            'email'=>$user->email,
        ]));


        return $user;
    }


    public function update(UserUpdateRequest $request, User $user)
    {
        $this->fill($request, $user);
        $user->save();

        return $user;
    }

    public function destroy(User $user)
    {
        $user->delete();

        return ['ok'];
    }

    public function fill(Request $request, User $user)
    {
        $user->id = $request->input('id');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->city = $request->input('city');
        $user->state = $request->input('state');
        $user->country = $request->input('country');

        if ($request->has('password'))
            $user->password = bcrypt($request->input('password'));
    }
}
