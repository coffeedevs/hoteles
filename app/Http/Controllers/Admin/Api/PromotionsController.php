<?php

namespace App\Http\Controllers\Admin\Api;

use App\Definitions\PromotionDefinition;
use App\Http\Requests\PromotionStoreRequest;
use App\Http\Requests\PromotionUpdateRequest;
use App\Promotion;
use App\Services\ImageService;
use App\Transformers\PromotionTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromotionsController extends Controller
{
    public function index(Request $request)
    {
        $promotions = Promotion::query();
        if ($search = $request->get('search'))
            $promotions->where('name', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%');

        if ($orderName = $request->get('order_name'))
            $promotions->orderBy($orderName, $request->get('order_type'));
        $promotions = $promotions->paginate(5);

        return PromotionTransformer::prepare($promotions);
    }

    public function store(PromotionStoreRequest $request)
    {
        $promotion = new Promotion();
        $promotion->image = '';
        $this->fill($request, $promotion);
        $promotion->save();

        $promotion->rooms()->delete();
        foreach($request->input('rooms') as $roomId){
            $promotion->rooms()->attach($roomId);
        }

        return $promotion;
    }

    public function update(PromotionUpdateRequest $request, Promotion $promotion)
    {
        $this->fill($request, $promotion);
        $promotion->save();
        return $promotion;
    }

    public function destroy(Promotion $promotion)
    {
        $promotion->delete();
        return ['ok'];
    }

    public function fill(Request $request, Promotion $promotion)
    {
        $promotion->name = $request->input('name');
        $promotion->description = $request->input('description');
        $promotion->subtitle = $request->input('subtitle');
        $promotion->price = $request->input('price');
        $promotion->exclusive_price = $request->input('exclusive_price');
        $promotion->nights = $request->input('nights');
        $promotion->people = $request->input('people');
        $promotion->start_at = Carbon::createFromFormat('d/m/Y', $request->input('start_at'));
        $promotion->finish_at = Carbon::createFromFormat('d/m/Y', $request->input('finish_at'));

        if ($request->hasFile('image')) {
            $file = $this->makeImageService()->savePicture($request->file('image'));
            $promotion->image = $file['name'];
        }


    }

    private function makeImageService()
    {
        $imageService = new ImageService;
        $imageService->setImagePath(PromotionDefinition::IMAGES_PATH);
        $imageService->setThumbsPath(PromotionDefinition::THUMBS_PATH);

        return $imageService;
    }
}
