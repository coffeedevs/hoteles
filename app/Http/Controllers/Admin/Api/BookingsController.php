<?php

namespace App\Http\Controllers\Admin\Api;

use App\Booking;
use App\Definitions\BookingDefinition;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookingStoreRequest;
use App\Http\Requests\BookingUpdateRequest;
use App\Transformers\BookingTransformer;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    public function index(Request $request)
    {
        $bookings = Booking::query();

        if ($search = $request->input('search'))
            $bookings->where('number', 'like', '%' . $search . '%');

        if ($orderName = $request->input('order_name'))
            $bookings->orderBy($orderName, $request->input('order_type'));
        else
            $bookings->orderBy('id','desc');
        $bookings = $bookings->paginate(5);

        return BookingTransformer::prepare($bookings);
    }

    public function store(BookingStoreRequest $request)
    {
        $booking = new Booking();
        $this->fill($request, $booking);
        $booking->save();

        return $booking;
    }

    public function update(BookingUpdateRequest $request, Booking $booking)
    {
        $this->fill($request, $booking);
        $booking->save();

        return $booking;
    }

    public function destroy(Booking $booking)
    {
        $booking->delete();

        return ['ok'];
    }

    public function fill(Request $request, Booking $booking)
    {
        $booking->number = $request->input('number');
        $booking->places = $request->input('places');
        $booking->double = $request->input('double');
        $booking->type = $request->input('type');
    }

    public function approve(Booking $booking)
    {
        $booking->status = BookingDefinition::APPROVED_STATUS;
        $booking->save();
    }

    public function disapprove(Booking $booking)
    {
        $booking->status = BookingDefinition::REJECTED_STATUS;
        $booking->save();
    }
}
