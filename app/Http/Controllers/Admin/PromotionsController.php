<?php

namespace App\Http\Controllers\Admin;

use App\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromotionsController extends Controller
{
    public function index()
    {
        $rooms = Room::all();
        return view('app.admin.promotions.index')
            ->withRooms($rooms);
    }
}
