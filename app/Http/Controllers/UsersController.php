<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{
    public function edit($id)
    {
        $user = User::find($id);

        return view('app.web.account-edit')->with('user', $user);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'country' => 'required|string',
        ]);

        $user = User::find($id);
        $user->update($request->except('email','password'));
        $user->save();

        $request->session()->flash('edit', 'ok');
        return redirect()->route('web.index');
    }

    public function profile()
    {
        return view('app.web.account-edit')->with('user', auth()->user());
    }

    public function index()
    {
        return view('admin.users.index');
    }
}
