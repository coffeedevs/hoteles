<?php
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 5/11/2017
 * Time: 8:19 AM
 */

namespace App\Http\Controllers\Web;


use App\Http\Requests\ContactRequest;
use App\Mail\SendContact;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;

class ContactsController
{
    public function send(ContactRequest $request, Mailer $mailer)
    {
       $mailer->to('info@hotelsierradelospadres.com.ar')->send(new SendContact($request->all()));

       return redirect()->route('web.contacts')->withSuccess('Se ha enviado correctamente');
    }
}