<?php

namespace App\Http\Controllers\Web;

use App\Booking;
use App\Definitions\BookingDefinition;
use App\Http\Controllers\Controller;
use App\Mail\SendBooking;
use App\Price;
use App\Services\BookingService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class BookingsController extends Controller
{
    public function checkAvailability(Request $request)
    {
        $this->validate(request(), [
            'start_at' => 'required|date_format:d/m/Y',
            'finish_at' => 'required|date_format:d/m/Y',
        ]);

        /** @var BookingService $bookingService */
        $bookingService = app(BookingService::class);

        /** @var Carbon $carbon */
        $carbon = app(Carbon::class);
        $startAt = $carbon->createFromFormat('d/m/Y', $request->input('start_at'));
        $finishAt = $carbon->createFromFormat('d/m/Y', $request->input('finish_at'));

        $days = $startAt->diffInDays($finishAt);

        $rooms = $bookingService->getAvailableOptions();

        $spa = $request->input('spa') ? $request->input('spa') : 0;
        $kids = $request->input('kids') ? $request->input('kids') : 0;
        $massages = $request->input('massages') ? $request->input('massages') : 0;

        $prices = Price::first();

        $additionalLabel = '';
        $additionalDetail = [];
        if ($spa > 0) {
            $additionalLabel .= '<p>- SPA & RELAX '.$spa.' días </p>';
            $additionalDetail['spa'] = [
                'days' => $spa,
                'total' => $spa * $prices->spa
            ];
        }

        if ($kids > 0) {
            $additionalLabel .= '<p>- Kid\'s TIME '.$kids.' días </p>';
            $additionalDetail['kids'] = [
                'days' => $kids,
                'total' => $kids * $prices->kids
            ];
        }

        if ($massages > 0) {
            $additionalLabel .= '<p>- MASAJES '.$massages.' días</p>';
            $additionalDetail['massages'] = [
                'days' => $massages,
                'total' => $massages * $prices->massages
            ];
        }

        $additionalTotal = $spa * $prices->spa * $days + $kids * $prices->kids * $days + $massages * $prices->massages * $days;

        $options = [];

        $suggested = [];
        foreach ($rooms['preferred'] as $room) {
            $options[] = [
                'rooms' => $room['rooms'],
                'rooms_price' => $room['price'],
                'total' => $room['price'] + $additionalTotal,
                'type' => $room['rooms'][0]->type,
                'additional' => [
                    'detail' => $additionalDetail,
                    'label' => $additionalLabel,
                    'total' => $additionalTotal,
                ],
                'data' => $request->all(),
            ];
        }



        foreach ($rooms['suggested'] as $room) {
            $suggested[] = [
                'rooms' => $room['rooms'],
                'rooms_price' => $room['price'],
                'total' => $room['price'] + $additionalTotal,
                'type' => $room['rooms'][0]->type,
                'additional' => [
                    'label' => $additionalLabel,
                    'total' => $additionalTotal,
                ],
                'data' => $request->all(),
            ];
        }
        return view('app.web.reservations-options')->withOptions($options)->withSuggested($suggested);
    }

    public function history()
    {
        $bookings = auth()->user()->bookings;

//        foreach ($bookings as $booking) {
//            $additional = $booking->additionalData['label'];
////            dd($additional);
//        }


        return view('app.web.history')->withBookings($bookings);
    }

    public function confirmBooking(Request $request)
    {
        // Creo registro de reserva
        // redirecciono hacia el pago

        $data = json_decode($request->input('data'));

//        dd($data);

        $booking = new Booking;

        $booking->user_id = auth()->user()->id;
        $booking->start_at = Carbon::createFromFormat('d/m/Y',$data->data->start_at);
        $booking->finish_at = Carbon::createFromFormat('d/m/Y',$data->data->finish_at);
        $booking->children = 0;
        $booking->minors = 0;
        $booking->cradle = 0;
        $booking->observation = '';
        $booking->price=$data->total;
        $booking->type=$data->type;
        $booking->guests=$data->data->guests_number;
        $booking->additional=json_encode($data->additional);
        $booking->status=BookingDefinition::PENDING_STATUS;
        $booking->save();

        foreach($data->rooms as $room){
            $booking->rooms()->attach($room->id);
        }

        //enviar mail para notificar

        Mail::to(config('mail.contact'))->send(new SendBooking($booking));

        return redirect()->route('web.reservation-success');
    }


}
