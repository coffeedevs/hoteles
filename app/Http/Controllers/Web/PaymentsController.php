<?php

namespace App\Http\Controllers\Web;

use App\Booking;
use App\Definitions\BookingDefinition;
use App\Http\Controllers\Controller;
use App\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MP;

class PaymentsController extends Controller
{
    public function payBooking(Request $request)
    {

        $data = json_decode($request->input('data'));

        $booking = new Booking;

        $booking->user_id = auth()->user()->id;
        $booking->start_at = Carbon::createFromFormat('d/m/Y',$data->data->start_at);
        $booking->finish_at = Carbon::createFromFormat('d/m/Y',$data->data->finish_at);
        $booking->children = 0;
        $booking->minors = 0;
        $booking->cradle = 0;
        $booking->observation = '';
        $booking->price=$data->total;
        $booking->type=$data->type;
        $booking->guests=$data->data->guests_number;
        $booking->additional=json_encode($data->additional);
        $booking->save();

        foreach($data->rooms as $room){
            $booking->rooms()->attach($room->id);
        }

    }

    public function payPromotion(Promotion $promotion)
    {
        $rooms = $promotion->availableRooms();
        if(count($rooms))
            $room = array_pop($rooms);
        else
            abort(403,'No hay habitaciones disponibles para esta promoción');


        $booking = new Booking;

        $booking->user_id = auth()->user()->id;
        $booking->start_at = $promotion->start_at;
        $booking->finish_at = $promotion->finish_at;
        $booking->children = 0;
        $booking->minors = 0;
        $booking->cradle = 0;
        $booking->observation = '';
        $booking->price=$promotion->exclusive_price;
        $booking->type=$room->type;
        $booking->guests=$promotion->people;
        $booking->additional='{}';
        $booking->promotion_id=$promotion->id;
        $booking->status=BookingDefinition::PENDING_STATUS;
        $booking->save();

        $booking->rooms()->attach($room->id);


        $mp = new MP(config('mercadopago.client_id'), config('mercadopago.client_secret'));
        $mp->sandbox_mode(false);


        $preference_data = array(
            "items" => array(
                array(
                    "title" => $promotion->title,
                    "quantity" => 1,
                    "currency_id" => "ARS", // Available currencies at: https://api.mercadopago.com/currencies
                    "unit_price" => $promotion->exclusive_price
                ),
            ),
            "back_urls" => [
                'success' => route('web.payment-success',['booking_id'=>$booking->id]),
                'pending' => route('web.payment-pending',['booking_id'=>$booking->id]),
                'failure' => route('web.payment-failure',['booking_id'=>$booking->id]),
            ],
            'external_reference' => 'promotion:' . $promotion->id.',booking:'.$booking->id,
        );

        $preference = $mp->create_preference($preference_data);


        return redirect($preference['response']['init_point']);
    }

    public function paymentSuccess()
    {
        return view('app.web.payment-result')->withResult('success');
    }

    public function paymentPending()
    {
        return view('app.web.payment-result')->withResult('pending');
    }

    public function paymentFailure()
    {
        return view('app.web.payment-result')->withResult('failure');
    }

    public function paymentReceived()
    {

    }
}
