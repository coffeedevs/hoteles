<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Promotion;
use Carbon\Carbon;
use MP;

class PromotionsController extends Controller
{
    public function getPromotions()
    {
        $promotions = Promotion::where('start_at','>',Carbon::now())->get();

        return view('app.web.packages', ['promotions' => $promotions]);
    }

    public function bookingPromotion(Promotion $promotion)
    {
        return view('app.web.reservations-promotions');
    }
}
