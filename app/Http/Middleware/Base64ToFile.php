<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyUploadedFile;

class Base64ToFile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach ($request->all() as $key => $value) {
            if (is_array($value))
                continue;
            if (strpos($value, 'data:image') !== false) {
                $value = str_replace('data:image/jpeg;base64,', '', $value);
                $value = str_replace('data:image/png;base64,', '', $value);
                $value = str_replace(' ', '+', $value);
                $data = base64_decode($value);
                $name = uniqid('tmp_',true).rand(100000,999999).'_base64.jpg';
                Storage::put($name, $data);
                $uploadedFile = UploadedFile::createFromBase(
                    new SymfonyUploadedFile(storage_path('app/'.$name), $name), true);
                $request->files->set($key, $uploadedFile);
            }
        }

        return $next($request);
    }
}
