<?php

namespace App\Http\Requests;

use App\Definitions\Room;
use Illuminate\Foundation\Http\FormRequest;

class RoomStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'=>'required|numeric|min:1|unique:rooms',
            'places'=>'required|numeric|min:1',
            'double'=>'required|numeric',
            'type'=>'required|in:'.Room::NORMAL_TYPE.','.Room::APART_TYPE,
        ];
    }

    public function messages()
    {
        return [
            'number.required'=>'Debe ingresar el número de la habitación',
            'number.min'=>'El número debe ser mayor a 0',
        ];
    }
}
