<?php

namespace App\Http\Requests;

use App\Definitions\Room;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class RoomUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $segments = Request::segments();
        return [
            'number'=>'required|numeric|min:1|unique:rooms,number,'.end($segments),
            'places'=>'required|numeric|min:1',
            'double'=>'required|numeric',
            'type'=>'required|in:'.Room::NORMAL_TYPE.','.Room::APART_TYPE,
        ];
    }
}
