<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PromotionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'description'=>'required',
            'image'=>'required',
            'price'=>'required',
            'start_at'=>'required',
            'finish_at'=>'required',
            'subtitle'=>'required',
            'exclusive_price'=>'required',
            'nights'=>'required|numeric',
            'people'=>'required|numeric',
        ];
    }
}
