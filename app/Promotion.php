<?php

namespace App;

use App\Definitions\BookingDefinition;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Promotion
 *
 * @property mixed image
 * @property mixed price
 * @property mixed name
 * @property mixed id
 * @property mixed description
 * @property array|string exclusive_price
 * @property mixed esclusive_price
 * @property mixed subtitle
 * @property array|string finish_at
 * @property array|string start_at
 * @property array|string nights
 * @property array|string people
 * @property mixed title
 * @mixin \Eloquent
 */
class Promotion extends Model
{
    protected $dates = ['created_at', 'updated_at', 'start_at', 'finish_at'];

    public function image()
    {
        return new ModelImage($this);
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

    public function isSpent()
    {
        return count($this->availableRooms()) <= 0;
    }

    public function availableRooms()
    {

        $rooms = $this->rooms;
        $availableRooms = [];
        foreach ($rooms as $room) {
            $booking = Booking::join('booking_room', 'bookings.id', '=', 'booking_room.booking_id')
                ->select('bookings.*', 'booking_room.room_id')
                ->where('start_at', '<', $this->finish_at)
                ->where('finish_at', '>', $this->start_at)
                ->where('room_id', $room->id)
                ->where('status','<>',BookingDefinition::REJECTED_STATUS)
                ->first();

            if (!$booking)
                $availableRooms[] = $room;

        }

        return $availableRooms;
    }
}
