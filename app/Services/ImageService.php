<?php
namespace App\Services;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImageService
{
    private $imagesPath;
    
    private $thumbsPath;

    private $smallSize = 200;

    private $largeSize = 800;

    /**
     * @return mixed
     */
    public function getImagePath()
    {
        return $this->imagesPath;
    }

    /**
     * @param mixed $imagesPath
     */
    public function setImagePath($imagesPath)
    {
        $this->imagesPath = $imagesPath;
    }

    /**
     * @return mixed
     */
    public function getThumbsPath()
    {
        return $this->thumbsPath;
    }

    /**
     * @param mixed $thumbsPath
     */
    public function setThumbsPath($thumbsPath)
    {
        $this->thumbsPath = $thumbsPath;
    }

    private function getFileExtensionFromFile($file)
    {
        $fileExt = 'jpg';
        if ($file->getMimeType() == 'image/png')
            return 'png';
        elseif ($file->getMimeType() == 'image/jpeg')
            return 'jpg';
        else abort(500, 'Archivo no permitido');
    }

    private function makeFolderIfNotExist($path)
    {
        if (!file_exists(public_path() . $path)) {
            File::makeDirectory(public_path() . $path, $mode = 0777, true, true);
        }
    }

    private function makeLargeImage($img)
    {
        $this->makeImage($img, $this->largeSize);
    }

    private function makeSmallImage($img)
    {
        $this->makeImage($img, $this->smallSize);
    }

    private function makeImage($img, $size)
    {
        // Guardo imagen en tamaño grande
        if ($img->width() > $size)
            $img->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
            });
    }

    public function savePicture( $file)
    {
        $fileExt = $this->getFileExtensionFromFile($file);

        // Creao carpetas si no existen
        $this->makeFolderIfNotExist($this->imagesPath);
        $this->makeFolderIfNotExist($this->thumbsPath);

        // Creo el nombre del archivo
        $fileName = round(microtime(true)) .rand(100000,9999999). '.' . $fileExt;


        /** @var UploadedFile $file */
        $file->storeAs($this->imagesPath, $fileName, 'files');


        return [
            'name'=>$fileName,
            'type'=>$file->getMimeType(),
        ];
    }
}