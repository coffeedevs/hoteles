<?php

namespace App\Services;

use App\Booking;
use App\Definitions\BookingDefinition;
use App\Price;
use App\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookingService
{
    private $request;


    public function __construct(Request $request)
    {
        $this->request = $request;

    }

    public function options()
    {
        $normal = [
            1 => [
                [2]
            ],
            2 => [
                [2]
            ],
            3 => [
                [2, 1]
            ],
            4 => [
                [2, 2], [3, 1]
            ],
            5 => [
                [3, 2]
            ],
            6 => [
                [3, 3], [2, 2, 2]
            ],
            7 => [
                [3, 2, 2]
            ],
            8 => [
                [3, 3, 2]
            ],
            9 => [
                [3, 3, 3]
            ],
            10 => [
                [3, 3, 2, 2]
            ]
        ];

        $apart = [
            1 => [
                [4]
            ],
            2 => [
                [4]
            ],
            3 => [
                [4]
            ],
            4 => [
                [4]
            ],
            5 => [
                [6]
            ],
            6 => [
                [6]
            ],
            7 => [
                [4, 4], [4, 6]
            ],
            8 => [
                [4, 4], [6, 4]
            ],
            9 => [
                [6, 4]
            ],
            10 => [
                [6, 4]
            ],
        ];

        return $options = [
            'apart' => $apart,
            'normal' => $normal,
        ];
    }

    public function getAvailableOptions()
    {
        $type = $this->request->input('room_type');

        $preferredRooms = $this->getPreferredRooms($type);

        $suggestedRooms = $this->getPreferredRooms($type == \App\Definitions\Room::NORMAL_TYPE ? \App\Definitions\Room::APART_TYPE : \App\Definitions\Room::NORMAL_TYPE);

        return [
            'preferred' => $preferredRooms,
            'suggested' => $suggestedRooms,
        ];
    }

    private function getAvailableRooms($type)
    {
        $startAt = Carbon::createFromFormat('d/m/Y', $this->request->input('start_at'));
        $finishAt = Carbon::createFromFormat('d/m/Y', $this->request->input('finish_at'));

        $bookings = Booking::where('start_at', '<', $finishAt)->where('finish_at', '>', $startAt)->where('status', '<>', BookingDefinition::REJECTED_STATUS)->get();

        $usedRooms = [];
        foreach ($bookings as $booking) {
            if ($booking->rooms)
                foreach ($booking->rooms as $room) {
                    $usedRooms[] = $room->id;
                }
        }

        return Room::whereNotIn('id', $usedRooms)->orderBy('places', 'desc')->orderBy('double', 'desc')->where('type', $type)->get();
    }

    private function getPreferredRooms($type)
    {
        /** @var Carbon $carbon */
        $carbon = app(Carbon::class);
        $startAt = $carbon->createFromFormat('d/m/Y', $this->request->input('start_at'));
        $finishAt = $carbon->createFromFormat('d/m/Y', $this->request->input('finish_at'));

        $days = $startAt->diffInDays($finishAt);

        $daysByMonth = [];
        $initDate = $startAt;

        $count = 0;

        while ($initDate <= $finishAt) {
            if ($initDate->day == 1)
                $count = 1;
            else
                $count++;

            $daysByMonth[$initDate->month] = $count;
            $initDate = $initDate->addDay(1);
        }



        $bedType = $this->request->input('tipo_cama');
        $availableRooms = $this->getAvailableRooms($type);
        $guestsNumber = $this->request->input('guests_number');

        $options = !empty($this->options()[$type][$guestsNumber]) ? $this->options()[$type][$guestsNumber] : [];

//dd($options);
        $results = [];

        foreach ($options as $option) {
            $rooms = $this->getRoomsIfIsAvailable($availableRooms, $option);

            if ($rooms) {
                $results[] = [
                    'rooms' => $rooms,
                    'price' => $this->calcBasePrice($rooms, $daysByMonth),
                ];

            }

        }

        /*Una vez que evalue cuales combinaciones puedo usar, voy a preguntar por las camas si es que cumple el minimo
         * if ($bedType == 'double' || ($bedType == 'single' && $availableRoom->double == 0))
                    if ($guestsNumber >= $availableRoom->places) {
                        $double = true;
                        $guestsNumber -= $availableRoom->places;
                        $roomList[] = $availableRoom;
                    }
         * */


        return $results;
    }

    private function calcBasePrice($rooms, $days)
    {
        $prices = Price::all();
        $price = 0;

        foreach($days as $key => $d){
            foreach ($rooms as $room) {
                if ($room->type == 'normal') {
                    if ($room->places == 2) {
                        $price += $prices[$key-1]->normal2 * $d;
                    } else if ($room->places == 3) {
                        $price += $prices[$key-1]->normal3 * $d;
                    }
                } else {
                    if ($room->places == 4) {
                        $price += $prices[$key-1]->apart4 * $d;
                    } else if ($room->places == 6) {
                        $price += $prices[$key-1]->apart6 * $d;
                    }
                }
            }
        }


        return $price;
    }


    private function getRoomsIfIsAvailable($availableRooms, $option, $double = false)
    {
        $roomList = [];
        $hasDouble = false;
        foreach ($availableRooms as $availableRoom) {
            if ($double && !$hasDouble && $availableRoom->bedType == 'single')
                continue;

            if (count($option) && $availableRoom->places == $option[0]) {
                array_shift($option);
                $roomList[] = $availableRoom;
            }
            if (count($option) == 0)
                return $roomList;
        }
        return false;
    }
}