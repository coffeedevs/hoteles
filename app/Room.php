<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Room
 *
 * @property mixed number
 * @property mixed double
 * @property mixed places
 * @property mixed id
 * @property array|string type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Booking[] $Booking
 * @mixin \Eloquent
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at

 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereDouble($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room wherePlaces($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Room whereUpdatedAt($value)
 */
class Room extends Model
{
    public function Booking()
    {
        return $this->belongsToMany(Booking::class);
    }
}
