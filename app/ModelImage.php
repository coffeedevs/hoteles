<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelImage extends Model
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function link()
    {
        if ($this->model->image != null)
            return asset($this->model->images_path . $this->model->image);
        else return url("images/medium/placeholder.png");
    }

    public function __get($property)
    {
        if (method_exists(self::class, $property))
            return $this->$property();
        else if ($this->model->image != null)
            return url("images/$property/" . $this->model->image);
        else return url("images/$property/placeholder.jpg");
    }
}
