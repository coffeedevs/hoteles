<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Booking
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Room[] $rooms
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $start_at
 * @property string $finish_at
 * @property int $children
 * @property int $minors
 * @property int $cradle
 * @property string $observation
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $user
 * @property string additional
 * @property  guests
 * @property  type
 * @property  price
 * @property string status
 * @property mixed promotion_id
 * @property array|string guests
 * @property  type
 * @property array|string price
 * @property  guests
 * @property  type
 * @property  price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereChildren($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereCradle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereFinishAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereMinors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereObservation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Booking whereUserId($value)
 */
class Booking extends Model
{

    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getAdditionalDataAttribute()
    {
        return json_decode($this->attributes['additional'], true);
    }

    protected $dates = ['created_at', 'updated_at', 'start_at', 'finish_at'];

    public function getStatusTitleAttribute()
    {
        if ($this->status == self::STATUS_APPROVED) {
            return 'Aprobada';
        } elseif ($this->status == self::STATUS_REJECTED) {
            return 'Rechazada';
        } else return '';
    }
}
