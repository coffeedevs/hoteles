<?php
namespace App\Definitions;

class PromotionDefinition
{
    const IMAGES_PATH = '/images/promotions';
    const THUMBS_PATH = '/images/promotions/thumbs';
    const LARGE_SIZE = 1920;
    const SMALL_SIZE = 300;
}