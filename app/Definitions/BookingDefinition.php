<?php
namespace App\Definitions;

class BookingDefinition
{
    const PENDING_STATUS= 'pending';
    const PENDING_STATUS_LABEL = 'Pendiente';
    const APPROVED_STATUS= 'approved';
    const APPROVED_STATUS_LABEL = 'Aprobado';
    const REJECTED_STATUS= 'rejected';
    const REJECTED_STATUS_LABEL = 'Rechazado';

}