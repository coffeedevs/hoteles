<?php
namespace App\Definitions;

class Room
{
    const NORMAL_TYPE = 'normal';
    const NORMAL_TYPE_LABEL = 'Habitación Normal';
    const APART_TYPE = 'apart';
    const APART_TYPE_LABEL = 'Apart Hotel';


    public static function getTypes()
    {
        return [
            [
                'value'=>self::NORMAL_TYPE,
                'label'=>self::NORMAL_TYPE_LABEL,
            ],
            [
                'value'=>self::APART_TYPE,
                'label'=>self::APART_TYPE_LABEL,
            ]
        ];
    }
}