<?php

namespace App\Providers;

use App\Promotion;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('app.web.index', function ($view) {
            $view->with([
                'current_month' => Carbon::today()->formatLocalized('%B'),
                'promotions' => Promotion::latest()->take(2)->get(),
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
