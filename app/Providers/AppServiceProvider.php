<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!windows_os()) {
            setlocale(LC_ALL, ['es_ES', 'es-ES', 'esp', 'es_ES.UTF-8', 'Spanish_Spain.1252', 'WINDOWS-1252']);
            Carbon::setLocale('es');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
