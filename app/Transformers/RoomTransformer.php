<?php
/**
 * Created by PhpStorm.
 * User: facundo.goni
 * Date: 8/11/2016
 * Time: 6:49 PM
 */

namespace App\Transformers;

use App\Room;

class RoomTransformer extends BaseTransformer
{
    public function transform(Room $room)
    {
        return [
            'id'=>$room->id,
            'number'=>$room->number,
            'places'=>$room->places,
            'double'=>$room->double,
            'type'=>$room->type,
        ];
    }
}