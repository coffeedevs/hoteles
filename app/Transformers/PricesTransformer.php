<?php
/**
 * Created by PhpStorm.
 * User: facundo.goni
 * Date: 8/11/2016
 * Time: 6:49 PM
 */

namespace App\Transformers;

use App\Price;
use App\User;

class PricesTransformer extends BaseTransformer
{
    public function transform(Price $price)
    {
        return [
            'normal2'=>$price->normal2,
            'normal3'=>$price->normal3,
            'apart4'=>$price->apart4,
            'apart6'=>$price->apart6,
            'spa'=>$price->spa,
            'kids'=>$price->kids,
            'massages'=>$price->massages,
            'month'=>$price->month,
        ];
    }
}