<?php
/**
 * Created by PhpStorm.
 * User: facundo.goni
 * Date: 8/11/2016
 * Time: 6:49 PM
 */

namespace App\Transformers;

use App\Booking;

class BookingTransformer extends BaseTransformer
{
    public function transform(Booking $booking)
    {
        return [
            'id' => $booking->id,
            'user_id' => $booking->user_id,
            'user' => UserTransformer::prepare($booking->user),
            'user_name' => $booking->user->name,
            'start_at' => $booking->start_at->format('d/m/Y'),
            'finish_at' => $booking->finish_at->format('d/m/Y'),
            'guests' => $booking->guests,
            'status' => $booking->status,
            'price'=>$booking->price,
            'additional'=>json_decode($booking->additional),
            'created_at' => $booking->created_at->format('d/m/Y')
        ];
    }
}