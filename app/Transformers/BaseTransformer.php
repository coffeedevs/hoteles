<?php
/**
 * Created by PhpStorm.
 * User: facundo.goni
 * Date: 8/11/2016
 * Time: 5:14 PM
 */

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\TransformerAbstract;

class BaseTransformer extends TransformerAbstract
{
    /**
     * @param $object
     *
     * @return array|LengthAwarePaginator
     */
    public static function prepare($object)
    {
        $instance = new static();
        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());
        $item = null;


        if ($object instanceof Collection)
            $item = new FractalCollection($object, $instance);
        elseif ($object instanceof LengthAwarePaginator) {

            /** @var LengthAwarePaginator $object */
            $item = [
                "total" => $object->total(),
                "per_page" => $object->perPage(),
                "current_page" => $object->currentPage(),
                "last_page" => $object->lastPage(),
                "next_page_url" => $object->nextPageUrl(),
                "prev_page_url" => $object->previousPageUrl(),
                "from" => $object->firstItem(),
                "to" => $object->lastItem(),
                'data' => $manager->createData(new FractalCollection($object->getCollection(), $instance))->toArray()[ 'data' ],
            ];

            return $item;
        }
        else
            $item = new Item($object, $instance);

        return $manager->createData($item)->toArray();
    }
}