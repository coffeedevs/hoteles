<?php
/**
 * Created by PhpStorm.
 * User: facundo.goni
 * Date: 8/11/2016
 * Time: 6:49 PM
 */

namespace App\Transformers;

use App\User;

class UserTransformer extends BaseTransformer
{
    public function transform(User $user)
    {
        return [
            'id'    => $user->id,
            'name'  => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'city' => $user->city,
            'state' => $user->state,
            'country' => $user->country,
        ];
    }
}