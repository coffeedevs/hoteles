<?php
/**
 * Created by PhpStorm.
 * User: facundo.goni
 * Date: 8/11/2016
 * Time: 6:49 PM
 */

namespace App\Transformers;

use App\Promotion;
use App\Room;

class PromotionTransformer extends BaseTransformer
{
    public function transform(Promotion $promotion)
    {
        
        return [
            'id'=>$promotion->id,
            'name'=>$promotion->name,
            'subtitle'=>$promotion->subtitle,
            'price'=>$promotion->price,
            'exclusive_price'=>$promotion->exclusive_price,
            'image'=>$promotion->image,
            'description'=>$promotion->description,
            'start_at'=>$promotion->start_at->format('d/m/Y'),
            'finish_at'=>$promotion->finish_at->format('d/m/Y'),
            'nights'=>$promotion->nights,
            'people'=>$promotion->people,
            'image_large' => $promotion->image()->large,
            'image_medium' => $promotion->image()->medium,
            'image_small' => $promotion->image()->small,
            'rooms'=>$promotion->rooms->pluck('id'),
        ];
    }
}