<?php

use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'phone'          => $faker->phoneNumber,
        'city'           => $faker->city,
        'state'          => '',
        'country'        => $faker->country,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Room::class, function (Faker $faker) {
    return [];
});

$factory->define(App\Booking::class, function (Faker $faker) {
    return [
        'user_id'     => User::query()->inRandomOrder()->first(),
        'start_at'    => Carbon::today()->addDays($faker->numberBetween(0, 5)),
        'finish_at'   => Carbon::today()->addDays($faker->numberBetween(10, 20)),
        'children'    => $faker->numberBetween(0, 5),
        'cradle'      => $faker->boolean(),
        'minors'      => $faker->boolean(),
        'observation' => $faker->sentence(),
    ];
});

$factory->define(App\BookingRoom::class, function (Faker $faker) {
    return [];
});
