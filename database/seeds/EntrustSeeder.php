<?php

use App\Role;
use Illuminate\Database\Seeder;

class EntrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Role::truncate();
        Role::create([
            'name'         => 'admin',
            'display_name' => 'Administrador',
            'description'  => 'Administrador del sistema de reservas',
        ]);
        Schema::enableForeignKeyConstraints();
    }
}
