<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        $admin = factory(User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@hoteles.com',
            'phone' => '12345',
            'city' => '12345',
            'state' => '12345',
            'country' => '12345',
            'password' => bcrypt('preview123'),
        ]);

        $user = User::find(1);
        $adminRole = Role::find(1);

        $user->attachRole($adminRole);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
