<?php

use App\Room;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $number = 1;
        // 3 normal rooms with double
        for ($i = 0; $i < 3; $i++) {
            factory(Room::class)->create([
                'number' => $number,
                'type' => App\Definitions\Room::NORMAL_TYPE,
                'places' => 3,
                'double' => 1,
            ]);
            $number++;
        }
        // 3 normal rooms with singles
        for ($j = 0; $j < 3; $j++) {
            factory(Room::class)->create([
                'number' => $number,
                'type' => App\Definitions\Room::NORMAL_TYPE,
                'places' => 2,
                'double' => 0,
            ]);
            $number++;
        }
        // 3 apart for 4 people
        for ($k = 0; $k < 3; $k++) {
            factory(Room::class)->create([
                'number' => $number,
                'type' => App\Definitions\Room::APART_TYPE,
                'places' => 4,
                'double' => 1,
            ]);
            $number++;
        }
        // 1 apart for 6 people

        factory(Room::class)->create([
            'number' => $number,
            'type' => App\Definitions\Room::APART_TYPE,
            'places' => 6,
            'double' => 1,
        ]);

    }
}
