<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner = new Role();
        $owner->name         = 'admin';
        $owner->display_name = 'Admin'; // optional
        $owner->description  = 'Admin del sistema'; // optional
        $owner->save();

        $admin = new Role();
        $admin->name         = 'usuario';
        $admin->display_name = 'Usuario'; // optional
        $admin->description  = 'Usuario del sistema'; // optional
        $admin->save();
    }
}
