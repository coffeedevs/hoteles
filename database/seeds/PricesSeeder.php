<?php

use App\Price;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('prices')->truncate();

        $months = [
            'january',
            'february',
            'march',
            'april',
            'may',
            'june',
            'july',
            'august',
            'september',
            'october',
            'november',
            'december',
        ];

        foreach ($months as $month) {
            $price = new Price;
            $price->normal2 = 0;
            $price->normal3 = 0;
            $price->apart4 = 0;
            $price->apart6 = 0;
            $price->spa = 0;
            $price->kids = 0;
            $price->massages = 0;
            $price->month = $month;
            $price->save();
        }


        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
