<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Promotion;
use App\ModelImage;

class PromotionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $promotion = new Promotion();
        $promotion->name = "DESCANSO EN FAMILIA";
        $promotion->subtitle = "OPORTUNIDAD FINES DE SEMANA LARGO";
        $promotion->description = "Reservando dos habitaciones de HOTEL, 30% OFF!";
        $promotion->price = 1500;
        $promotion->exclusive_price = 800;
        $promotion->image = "images/medium/placeholder.png";
        $promotion->start_at = Carbon::today();
        $promotion->finish_at = Carbon::today()->addMonth();
        $promotion->nights = 3;
        $promotion->people = 4;

        $promotion->save();

        $promotion2 = new Promotion();
        $promotion2->name = "TRES POR DOS \"FIN DE SEMANA ESPECIAL\"";
        $promotion2->subtitle = "PAQUETE ESPECIAL";
        $promotion2->description = "Escapate con tu pareja un fin de semana. Hacé tu check-in un Viernes, check-out en Domingo siguiente y disfrutá!";
        $promotion2->price = 1500;
        $promotion2->exclusive_price = 800;
        $promotion2->image = "images/medium/placeholder.png";
        $promotion2->start_at = Carbon::today();
        $promotion2->finish_at = Carbon::today()->addMonth();
        $promotion2->nights = 2;
        $promotion2->people = 2;

        $promotion2->save();

        $promotion3 = new Promotion();
        $promotion3->name = "NOCHE DE BODAS";
        $promotion3->subtitle = "PROMOCIÓN ROMANTICA";
        $promotion3->description = "Sólo para recién casados! Incluye desayuno especial completo para 2, kit romántico, masajes privados y champagne en la habitación.";
        $promotion3->price = 1500;
        $promotion3->exclusive_price = 800;
        $promotion3->image = "images/medium/placeholder.png";
        $promotion3->start_at = Carbon::today();
        $promotion3->finish_at = Carbon::today()->addMonth();
        $promotion3->nights = 1;
        $promotion3->people = 2;

        $promotion->save();
    }
}
