<?php

use App\Booking;
use Illuminate\Database\Seeder;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Booking::truncate();
        factory(Booking::class, 10)->create();
        Schema::enableForeignKeyConstraints();

    }
}
